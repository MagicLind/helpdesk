package by.training.novikov.email.service.impl;

import by.training.novikov.email.service.EmailService;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.service.UserService;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Locale;

@Service
public class EmailServiceImpl implements EmailService {

  public static final String SENDER = "HelpDesk";
  private static final String EMAIL_TEMPLATE_TICKET = "html/ticketNotice";
  private static final String ENCODING = "UTF-8";

  private final JavaMailSender mailSender;
  private final TemplateEngine htmlTemplateEngine;
  private final UserService userService;
  private final MessageSource messageSource;

  public EmailServiceImpl(
      JavaMailSender mailSender,
      TemplateEngine htmlTemplateEngine,
      UserService userService,
      MessageSource messageSource) {
    this.mailSender = mailSender;
    this.htmlTemplateEngine = htmlTemplateEngine;
    this.userService = userService;
    this.messageSource = messageSource;
  }

  @Override
  @Async
  public void sendMessageTicketCreated(Ticket ticket) {
    List<User> managers = userService.getAllMangers();
    String[] emails = managers.stream().map(User::getEmail).toArray(String[]::new);

    sendMessage("New ticket for approval", "Mangers",
        "ticket.created", ticket.getId(), emails);
  }

  @Override
  @Async
  public void sendMessageTicketCanceledByEngineer(Ticket ticket) {
    String[] emails = {ticket.getOwner().getEmail(), ticket.getApprover().getEmail()};

    sendMessage("Ticket was cancelled", "Users",
        "canceled.by.engineer", ticket.getId(), emails);
  }

  @Override
  @Async
  public void sendMessageTicketCanceledByManager(Ticket ticket) {
    sendMessage("Ticket was cancelled", getName(ticket.getOwner()),
        "canceled", ticket.getId(), ticket.getOwner().getEmail());
  }

  @Override
  @Async
  public void sendMessageTicketApproved(Ticket ticket) {
    List<User> recipients = userService.getAllEngineers();
    recipients.add(ticket.getOwner());
    String[] emails = recipients.stream().map(User::getEmail).toArray(String[]::new);

    sendMessage("Ticket was approved", "Users",
        "approved", ticket.getId(), emails);
  }

  @Override
  @Async
  public void sendMessageTicketDeclined(Ticket ticket) {
    sendMessage("Ticket was declined", getName(ticket.getOwner()),
        "declined", ticket.getId(), ticket.getOwner().getEmail());
  }

  @Override
  @Async
  public void sendMessageTicketDone(Ticket ticket) {
    sendMessage("Ticket was done", getName(ticket.getOwner()),
        "done", ticket.getId(), ticket.getOwner().getEmail());
  }

  @Override
  @Async
  public void sendMessageFeedbackProvided(Ticket ticket) {
    sendMessage("Feedback was provided", getName(ticket.getAssignee()),
        "feedback", ticket.getId(), ticket.getAssignee().getEmail());
  }

  private void sendMessage(String subject, String recipient, String messageSourceName, Long ticketId, String... to) {
    Context ctx = new Context();
    ctx.setVariable("recipient", recipient);
    ctx.setVariable("ticketId", ticketId);
    ctx.setVariable("message", messageSource.getMessage(messageSourceName, new Object[]{ticketId}, Locale.ENGLISH));

    MimeMessage mimeMessage = this.mailSender.createMimeMessage();
    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, ENCODING);

    try {
      message.setSubject(subject);
      message.setFrom(SENDER);
      message.setTo(to);
      String htmlContent = this.htmlTemplateEngine.process(EMAIL_TEMPLATE_TICKET, ctx);
      message.setText(htmlContent, true);
    } catch (MessagingException e) {
      e.printStackTrace();
    }

    this.mailSender.send(mimeMessage);
  }

  private String getName(User user) {
    return String.format("%s %s", user.getFirstName(), user.getLastName());
  }
}
