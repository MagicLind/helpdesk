package by.training.novikov.email.service;

import by.training.novikov.ticket.domain.Ticket;

public interface EmailService {

  void sendMessageTicketCreated(Ticket ticket);

  void sendMessageTicketCanceledByEngineer(Ticket ticket);

  void sendMessageTicketCanceledByManager(Ticket ticket);

  void sendMessageTicketApproved(Ticket ticket);

  void sendMessageTicketDeclined(Ticket ticket);

  void sendMessageTicketDone(Ticket ticket);

  void sendMessageFeedbackProvided(Ticket ticket);
}
