package by.training.novikov.attachment.converter;

import by.training.novikov.attachment.domain.Attachment;
import by.training.novikov.attachment.dto.AttachmentDto;
import by.training.novikov.ticket.domain.Ticket;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class AttachmentConverter {

  public Attachment toEntity(AttachmentDto attachmentDto, Ticket ticket) throws IOException {
    return new Attachment(attachmentDto.getBytes(), ticket, attachmentDto.getName());
  }
}
