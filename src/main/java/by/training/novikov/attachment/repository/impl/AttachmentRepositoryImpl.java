package by.training.novikov.attachment.repository.impl;

import by.training.novikov.attachment.domain.Attachment;
import by.training.novikov.attachment.repository.AttachmentRepository;
import by.training.novikov.common.dao.AbstractGenericDao;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class AttachmentRepositoryImpl extends AbstractGenericDao<Attachment>
    implements AttachmentRepository {

  protected AttachmentRepositoryImpl(SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  @Override
  public Optional<Attachment> findByTicketId(Long ticketId) {
    return findByParameter(ticketId, "ticket", "id");
  }
}
