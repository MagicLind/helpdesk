package by.training.novikov.attachment.repository;

import by.training.novikov.attachment.domain.Attachment;

import java.util.Optional;

public interface AttachmentRepository {

  Optional<Attachment> findByTicketId(Long ticketId);

  Attachment save(Attachment attachment);

  void delete(Attachment attachment);
}
