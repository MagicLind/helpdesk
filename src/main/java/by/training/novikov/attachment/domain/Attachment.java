package by.training.novikov.attachment.domain;

import by.training.novikov.ticket.domain.Ticket;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(indexes = @Index(columnList = "ticket_id"))
public class Attachment implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(nullable = false)
  private String name;

  @OneToOne
  @JoinColumn(nullable = false)
  private Ticket ticket;

  @Lob
  @Column(nullable = false)
  private byte[] blob;

  public Attachment() {
  }

  public Attachment(byte[] blob, Ticket ticket, String name) {
    this.blob = blob;
    this.ticket = ticket;
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public byte[] getBlob() {
    return blob;
  }

  public void setBlob(byte[] blob) {
    this.blob = blob;
  }

  public Ticket getTicket() {
    return ticket;
  }

  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Attachment that = (Attachment) o;
    return Arrays.equals(blob, that.blob) && Objects.equals(ticket, that.ticket) && Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(ticket, name);
    result = 31 * result + Arrays.hashCode(blob);
    return result;
  }
}
