package by.training.novikov.attachment.service;

import by.training.novikov.attachment.domain.Attachment;
import by.training.novikov.attachment.dto.AttachmentDto;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.io.IOException;

@Validated
public interface AttachmentService {

  Attachment getEntityByTicketId(@NotNull(message = "Tickets id should not be null")Long ticketId);

  Attachment create(AttachmentDto attachmentDto,@NotNull(message = "Tickets id should not be null") Long ticketId) throws IOException;

  void deleteByTicketId(@NotNull(message = "Tickets id should not be null")Long ticketId);
}
