package by.training.novikov.attachment.service.impl;

import by.training.novikov.attachment.converter.AttachmentConverter;
import by.training.novikov.attachment.domain.Attachment;
import by.training.novikov.attachment.dto.AttachmentDto;
import by.training.novikov.attachment.repository.AttachmentRepository;
import by.training.novikov.attachment.service.AttachmentService;
import by.training.novikov.common.exception.NotFoundException;
import by.training.novikov.history.service.HistoryService;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.exception.TicketForbiddenException;
import by.training.novikov.ticket.service.TicketService;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Optional;

@Service
public class AttachmentServiceImpl implements AttachmentService {

  private final AttachmentRepository attachmentRepository;
  private final AttachmentConverter attachmentConverter;
  private final TicketService ticketService;
  private final HistoryService historyService;
  private final UserService userService;

  public AttachmentServiceImpl(
      AttachmentRepository attachmentRepository,
      AttachmentConverter attachmentConverter,
      TicketService ticketService,
      HistoryService historyService,
      UserService userService) {
    this.attachmentRepository = attachmentRepository;
    this.attachmentConverter = attachmentConverter;
    this.ticketService = ticketService;
    this.historyService = historyService;
    this.userService = userService;
  }

  @Override
  @Transactional
  public Attachment getEntityByTicketId(Long ticketId) {
    Attachment attachment =
        attachmentRepository
            .findByTicketId(ticketId)
            .orElseThrow(() -> new NotFoundException("Attachment with id of ticket:" + ticketId + " not found!"));
    ticketService.checkCredential(attachment.getTicket().getId());

    return attachment;
  }

  @Override
  @Transactional
  public Attachment create(AttachmentDto attachmentDto, Long ticketId) throws IOException {
    Optional<Attachment> oldAttachment = attachmentRepository.findByTicketId(ticketId);
    Ticket ticketOwner = ticketService.getEntity(ticketId);
    checkUserIsOwner(ticketOwner, userService.getCurrentUser());
    oldAttachment.ifPresent(attachment -> deleteOldAttachment(attachment, ticketOwner));
    Attachment attachment = attachmentConverter.toEntity(attachmentDto, ticketOwner);
    attachmentRepository.save(attachment);
    historyService.addHistoryFileAttached(attachment.getName(), ticketOwner);

    return attachment;
  }

  @Override
  @Transactional
  public void deleteByTicketId(Long ticketId) {
    Attachment attachment = getEntityByTicketId(ticketId);
    checkUserIsOwner(attachment.getTicket(), userService.getCurrentUser());
    deleteOldAttachment(attachment, attachment.getTicket());
  }

  private void deleteOldAttachment(Attachment oldAttachment, Ticket ticketOwner) {
    attachmentRepository.delete(oldAttachment);
    historyService.addHistoryFileRemoved(oldAttachment.getName(), ticketOwner);
  }

  private void checkUserIsOwner(Ticket ticket, User user) {
    if (!ticket.getOwner().equals(user)) {
      throw new TicketForbiddenException("User is not owner of the ticket");
    }
  }
}
