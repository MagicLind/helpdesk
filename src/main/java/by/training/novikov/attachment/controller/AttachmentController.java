package by.training.novikov.attachment.controller;

import by.training.novikov.attachment.domain.Attachment;
import by.training.novikov.attachment.dto.AttachmentDto;
import by.training.novikov.attachment.service.AttachmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
@RequestMapping("/api/v1/tickets/{ticketId}/attachments")
public class AttachmentController {

  private static final String HEADER_PARAMETER_NAME = "Content-Disposition";

  private final AttachmentService attachmentService;

  public AttachmentController(AttachmentService attachmentService) {
    this.attachmentService = attachmentService;
  }

  @GetMapping
  public ResponseEntity<String> download(@PathVariable Long ticketId, HttpServletResponse response) throws IOException {
    Attachment attachment = attachmentService.getEntityByTicketId(ticketId);

    response.setContentLength(attachment.getBlob().length);
    response.setHeader(HEADER_PARAMETER_NAME, "attachment; filename=\"" + attachment.getName() + "\"");
    FileCopyUtils.copy(attachment.getBlob(), response.getOutputStream());

    return ResponseEntity.status(OK).build();
  }

  @PostMapping(consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<String> upload(@PathVariable Long ticketId,
                                       @ModelAttribute @Valid AttachmentDto attachmentDto) throws IOException {
    attachmentService.create(attachmentDto, ticketId);

    String currentUri = ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString();

    return ResponseEntity.status(CREATED)
      .body(currentUri);
  }

  @DeleteMapping
  public ResponseEntity<String> delete(@PathVariable Long ticketId) {
    attachmentService.deleteByTicketId(ticketId);
    return ResponseEntity.status(NO_CONTENT).build();
  }
}
