package by.training.novikov.attachment.dto.validator;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FileConstraintValidator implements ConstraintValidator<File, MultipartFile> {

  private String extensions;
  private Long fileMaxSize;

  @Override
  public void initialize(File file) {
    extensions = file.extensions();
    fileMaxSize = file.fileMaxSize();
  }

  @Override
  public boolean isValid(MultipartFile file, ConstraintValidatorContext cxt) {
    return validateFile(file);
  }

  private boolean validateFile(MultipartFile file) {
    if(file == null){
      return false;
    }

    String fileName = file.getOriginalFilename();
    assert fileName != null;
    if (fileName.equals("") || file.getSize() == 0) {
      return false;
    }

    return validateFileSize(file) && (validateFileExtension(fileName));
  }

  private boolean validateFileSize(MultipartFile file) {
    return file.getSize() <= fileMaxSize;
  }

  private boolean validateFileExtension(String fileName) {
    return fileName.matches(".*\\." + extensions);
  }


}
