package by.training.novikov.attachment.dto;

import by.training.novikov.attachment.dto.validator.File;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;

public class AttachmentDto implements Serializable {

  private static final long FILE_MAX_SIZE = 5242880L; // 5MB
  private static final String AVAILABLE_EXTENSIONS = "(pdf|doc|docx|png|jpeg|jpg)";

  private Long id;

  @File(fileMaxSize = FILE_MAX_SIZE, extensions = AVAILABLE_EXTENSIONS)
  @NotNull
  private MultipartFile file;

  public AttachmentDto() {}

  public AttachmentDto(Long id, MultipartFile file) {
    this.id = id;
    this.file = file;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return file.getOriginalFilename();
  }

  public byte[] getBytes() throws IOException {
    return file.getBytes();
  }

  public void setFile(MultipartFile file) {
    this.file = file;
  }

  public MultipartFile getFile() {
    return file;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AttachmentDto that = (AttachmentDto) o;
    return Objects.equals(file, that.file);
  }

  @Override
  public int hashCode() {
    return Objects.hash(file);
  }
}
