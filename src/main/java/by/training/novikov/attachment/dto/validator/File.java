package by.training.novikov.attachment.dto.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = FileConstraintValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface File {

  String message() default "{This file isn't supported or is null}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

  String extensions();

  long fileMaxSize();
}
