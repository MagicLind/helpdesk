package by.training.novikov.comment.domain;

import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.user.domain.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(indexes = @Index(columnList = "ticket_id"))
public class Comment implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(length = 500, nullable = false)
  private String text;

  @ManyToOne
  @JoinColumn(nullable = false)
  private User user;

  @ManyToOne
  @JoinColumn(nullable = false)
  private Ticket ticket;

  @Column(nullable = false)
  private Date date;

  public Comment() {
  }

  public Comment(String text, User user, Ticket ticket, Date date) {
    this.text = text;
    this.user = user;
    this.ticket = ticket;
    this.date = date;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Ticket getTicket() {
    return ticket;
  }

  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Comment comment = (Comment) o;
    return Objects.equals(text, comment.text) && Objects.equals(user, comment.user) && Objects.equals(ticket, comment.ticket) && Objects.equals(date, comment.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(text, user, ticket, date);
  }
}
