package by.training.novikov.comment.converter;

import by.training.novikov.comment.domain.Comment;
import by.training.novikov.comment.dto.CommentDto;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.user.converter.UserConverter;
import by.training.novikov.user.service.UserService;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CommentConverter {

  private final UserService userService;
  private final UserConverter userConverter;

  public CommentConverter(UserService userService, UserConverter userConverter) {
    this.userService = userService;
    this.userConverter = userConverter;
  }

  public Comment toEntity(CommentDto commentDto, Ticket ticket) {
    return new Comment(
      commentDto.getText(),
      userService.getCurrentUser(),
      ticket,
      Date.from(Calendar.getInstance().toInstant()));
  }

  public CommentDto fromEntity(Comment entity) {
    return new CommentDto(
      entity.getText(),
      userConverter.fromEntity(entity.getUser()),
      entity.getDate());
  }

  public List<CommentDto> fromListEntity(List<Comment> comments) {
    return comments.stream().map(this::fromEntity).collect(Collectors.toList());
  }
}
