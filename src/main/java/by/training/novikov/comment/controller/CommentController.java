package by.training.novikov.comment.controller;

import by.training.novikov.comment.dto.CommentDto;
import by.training.novikov.comment.service.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1/tickets/{ticketId}/comments")
public class CommentController {

  private final CommentService commentService;

  public CommentController(CommentService commentService) {
    this.commentService = commentService;
  }

  @GetMapping(value = "/default", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<CommentDto>> getDefault(@PathVariable Long ticketId) {
    return ResponseEntity.ok(commentService.getDefault(ticketId));
  }

  @GetMapping(value = "/all", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<CommentDto>> getAll(@PathVariable Long ticketId) {
    return ResponseEntity.ok(commentService.getAll(ticketId));
  }

  @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<CommentDto> create(
    @PathVariable Long ticketId, @RequestBody @Valid CommentDto commentDto) {
    commentService.create(commentDto, ticketId);
    return ResponseEntity.status(CREATED).body(commentDto);
  }
}
