package by.training.novikov.comment.dto;

import by.training.novikov.user.dto.UserDto;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class CommentDto implements Serializable {

  @Pattern(
    regexp = "[a-zA-Z0-9~.\"(),:;<>@\\[\\]!#$%&'*+-/=?^_`{|} ]*",
    message =
      "The comment is allowed only lowercase and uppercase English alpha characters,"
        + " digits and special characters")
  @NotBlank
  @Size(max = 500, message = "The max size of comment is 500 chapters")
  private String text;

  private UserDto user;
  private Date date;

  public CommentDto() {
  }

  public CommentDto(String text, UserDto user, Date date) {
    this.text = text;
    this.user = user;
    this.date = date;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(UserDto user) {
    this.user = user;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CommentDto that = (CommentDto) o;
    return Objects.equals(text, that.text) && Objects.equals(user, that.user) && Objects.equals(date, that.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(text, user, date);
  }
}
