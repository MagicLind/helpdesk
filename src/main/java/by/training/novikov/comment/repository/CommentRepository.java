package by.training.novikov.comment.repository;

import by.training.novikov.comment.domain.Comment;

import java.util.List;

public interface CommentRepository {

  List<Comment> getAllByTicketIdWithLimit(Long ticketId, Integer limit);

  List<Comment> getAllByTicketId(Long ticketId);

  Comment save(Comment comment);
}
