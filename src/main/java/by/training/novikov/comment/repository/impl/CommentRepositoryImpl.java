package by.training.novikov.comment.repository.impl;

import by.training.novikov.comment.domain.Comment;
import by.training.novikov.comment.repository.CommentRepository;
import by.training.novikov.common.dao.AbstractGenericDao;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentRepositoryImpl extends AbstractGenericDao<Comment> implements CommentRepository {

  protected CommentRepositoryImpl(SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  @Override
  public List<Comment> getAllByTicketIdWithLimit(Long ticketId, Integer limit) {
    return findAllByParameterWithLimit(ticketId, limit, "ticket", "id");
  }

  @Override
  public List<Comment> getAllByTicketId(Long ticketId) {
    return findAllByParameter(ticketId, "ticket", "id");
  }
}
