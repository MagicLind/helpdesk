package by.training.novikov.comment.service;

import by.training.novikov.comment.dto.CommentDto;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
public interface CommentService {

  void create(CommentDto commentDto, @NotNull(message = "Tickets id should not be null") Long ticketId);

  List<CommentDto> getDefault(@NotNull(message = "Tickets id should not be null") Long ticketId);

  List<CommentDto> getAll(@NotNull(message = "Tickets id should not be null") Long ticketId);
}
