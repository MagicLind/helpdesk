package by.training.novikov.comment.service.impl;

import by.training.novikov.comment.converter.CommentConverter;
import by.training.novikov.comment.dto.CommentDto;
import by.training.novikov.comment.repository.CommentRepository;
import by.training.novikov.comment.service.CommentService;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.service.TicketService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

  private static final Integer DEFAULT_COUNT_COMMENTS = 5;

  private final CommentRepository commentRepository;
  private final CommentConverter commentConverter;
  private final TicketService ticketService;

  public CommentServiceImpl(CommentRepository commentRepository, CommentConverter commentConverter, TicketService ticketService) {
    this.commentRepository = commentRepository;
    this.commentConverter = commentConverter;
    this.ticketService = ticketService;
  }


  @Override
  @Transactional
  public void create(CommentDto commentDto, Long ticketId) {
    Ticket ticket = ticketService.getEntity(ticketId);
    commentRepository.save(commentConverter.toEntity(commentDto, ticket));
  }

  @Override
  @Transactional
  public List<CommentDto> getDefault(Long ticketId) {
    ticketService.checkCredential(ticketId);
    return commentConverter.fromListEntity(commentRepository.getAllByTicketIdWithLimit(ticketId, DEFAULT_COUNT_COMMENTS));
  }

  @Override
  @Transactional
  public List<CommentDto> getAll(Long ticketId) {
    ticketService.checkCredential(ticketId);
    return commentConverter.fromListEntity(commentRepository.getAllByTicketId(ticketId));
  }
}
