package by.training.novikov.user.dto;

import by.training.novikov.user.domain.enums.Role;

import java.io.Serializable;
import java.util.Objects;

public class UserDto implements Serializable {

  private Long id;
  private Role role;
  private String firstName;
  private String lastName;

  public UserDto() {
  }

  public static UserDto.Builder newBuilder() {
    return new UserDto().new Builder();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UserDto userDto = (UserDto) o;
    return role == userDto.role && Objects.equals(firstName, userDto.firstName) && Objects.equals(lastName, userDto.lastName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(role, firstName, lastName);
  }

  public class Builder {

    private Builder() {

    }

    public Builder setId(Long id) {
      UserDto.this.id = id;

      return this;
    }

    public Builder setRole(Role role) {
      UserDto.this.role = role;

      return this;
    }

    public Builder setFirstName(String firstName) {
      UserDto.this.firstName = firstName;

      return this;
    }

    public Builder setLastName(String lastName) {
      UserDto.this.lastName = lastName;

      return this;
    }

    public UserDto build() {
      return UserDto.this;
    }
  }
}

