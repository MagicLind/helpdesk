package by.training.novikov.user.service;

import by.training.novikov.user.domain.User;

import java.util.List;

public interface UserService {

  User getCurrentUser();

    List<User> getAllMangers();

    List<User> getAllEngineers();
}
