package by.training.novikov.user.service.impl;

import by.training.novikov.common.exception.NotFoundException;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.domain.enums.Role;
import by.training.novikov.user.repository.UserRepository;
import by.training.novikov.user.service.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  @Transactional
  public User getCurrentUser() {
    return userRepository
        .findByEmail(SecurityContextHolder.getContext().getAuthentication().getName())
        .orElseThrow(() -> new NotFoundException("Current user not found!"));
  }

  @Override
  @Transactional
  public List<User> getAllMangers() {
    return userRepository.findAllByRole(Role.MANAGER);
  }

  @Override
  @Transactional
  public List<User> getAllEngineers() {
    return userRepository.findAllByRole(Role.ENGINEER);
  }
}
