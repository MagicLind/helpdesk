package by.training.novikov.user.repository.impl;

import by.training.novikov.common.dao.AbstractGenericDao;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.domain.enums.Role;
import by.training.novikov.user.repository.UserRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends AbstractGenericDao<User> implements UserRepository {

  protected UserRepositoryImpl(SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  @Override
  @Transactional
  public Optional<User> findByEmail(String email) {
    Integer sessionStatistics = getSession().getStatistics().getEntityCount();
    return findByParameter(email, "email");
  }

  @Override
  public List<User> findAllByRole(Role role) {
    return findAllByParameter(role, "role");
  }
}
