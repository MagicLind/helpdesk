package by.training.novikov.user.repository;

import by.training.novikov.user.domain.User;
import by.training.novikov.user.domain.enums.Role;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

  Optional<User> findByEmail(String email);

  List<User> findAllByRole(Role role);
}
