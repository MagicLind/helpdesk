package by.training.novikov.user.domain.enums;

public enum Role {
  EMPLOYEE,
  MANAGER,
  ENGINEER
}
