package by.training.novikov.user.converter;

import by.training.novikov.user.domain.User;
import by.training.novikov.user.dto.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

  public UserDto fromEntity(User entity) {
    return UserDto.newBuilder()
      .setId(entity.getId())
      .setFirstName(entity.getFirstName())
      .setLastName(entity.getLastName())
      .setRole(entity.getRole())
      .build();
  }
}
