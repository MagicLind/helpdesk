package by.training.novikov.feedback.domain;

import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.user.domain.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(indexes = @Index(columnList = "ticket_id"))
public class Feedback implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @OneToOne
  @JoinColumn(nullable = false)
  private User user;

  @OneToOne
  @JoinColumn(nullable = false)
  private Ticket ticket;

  @Column(nullable = false)
  private Integer rate;

  @Column(nullable = false)
  private String text;

  @Column(nullable = false)
  private Date date;

  public Feedback() {
  }

  public static Builder newBuilder() {
    return new Feedback().new Builder();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Ticket getTicket() {
    return ticket;
  }

  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }

  public Integer getRate() {
    return rate;
  }

  public void setRate(Integer rate) {
    this.rate = rate;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Feedback feedback = (Feedback) o;
    return Objects.equals(user, feedback.user) && Objects.equals(ticket, feedback.ticket) && Objects.equals(rate, feedback.rate) && Objects.equals(text, feedback.text) && Objects.equals(date, feedback.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user, ticket, rate, text, date);
  }

  public class Builder {

    private Builder() {

    }

    public Builder setUser(User user) {
      Feedback.this.user = user;

      return this;
    }

    public Builder setTicket(Ticket ticket) {
      Feedback.this.ticket = ticket;

      return this;
    }

    public Builder setRate(Integer rate) {
      Feedback.this.rate = rate;

      return this;
    }

    public Builder setText(String text) {
      Feedback.this.text = text;

      return this;
    }

    public Builder setDate(Date date) {
      Feedback.this.date = date;

      return this;
    }

    public Feedback build() {
      return Feedback.this;
    }
  }
}
