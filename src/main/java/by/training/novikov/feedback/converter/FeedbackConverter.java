package by.training.novikov.feedback.converter;

import by.training.novikov.feedback.domain.Feedback;
import by.training.novikov.feedback.dto.FeedbackDto;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.user.converter.UserConverter;
import by.training.novikov.user.domain.User;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

@Component
public class FeedbackConverter {

  private final UserConverter userConverter;

  public FeedbackConverter(UserConverter userConverter) {
    this.userConverter = userConverter;
  }

  public Feedback toEntity(FeedbackDto feedbackDto, Ticket ticket, User feedbackSender) {
    return Feedback.newBuilder()
      .setTicket(ticket)
      .setDate(Date.from(Calendar.getInstance().toInstant()))
      .setRate(feedbackDto.getRate())
      .setText(feedbackDto.getText())
      .setUser(feedbackSender)
      .build();
  }

  public FeedbackDto fromEntity(Feedback entity) {
    return FeedbackDto.newBuilder()
      .setId(entity.getId())
      .setDate(entity.getDate())
      .setRate(entity.getRate())
      .setText(entity.getText())
      .setUser(userConverter.fromEntity(entity.getUser()))
      .build();
  }
}
