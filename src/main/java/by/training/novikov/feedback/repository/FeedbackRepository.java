package by.training.novikov.feedback.repository;

import by.training.novikov.feedback.domain.Feedback;

import java.util.Optional;

public interface FeedbackRepository {

  Optional<Feedback> findByTicketId(Long ticketId);

  Feedback save(Feedback feedback);
}
