package by.training.novikov.feedback.repository.impl;

import by.training.novikov.common.dao.AbstractGenericDao;
import by.training.novikov.feedback.domain.Feedback;
import by.training.novikov.feedback.repository.FeedbackRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class FeedbackRepositoryImpl extends AbstractGenericDao<Feedback> implements FeedbackRepository {

  protected FeedbackRepositoryImpl(SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  @Override
  public Optional<Feedback> findByTicketId(Long ticketId) {
    return findByParameter(ticketId, "ticket", "id");
  }
}
