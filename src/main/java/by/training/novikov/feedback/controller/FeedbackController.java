package by.training.novikov.feedback.controller;

import by.training.novikov.feedback.dto.FeedbackDto;
import by.training.novikov.feedback.service.FeedbackService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1/tickets/{ticketId}/feedbacks")
public class FeedbackController {

  private final FeedbackService feedbackService;

  public FeedbackController(FeedbackService feedbackService) {
    this.feedbackService = feedbackService;
  }

  @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<FeedbackDto> create(
    @RequestBody @Valid FeedbackDto feedbackDto, @PathVariable Long ticketId) {
    feedbackDto = feedbackService.create(feedbackDto, ticketId);

    return ResponseEntity.status(CREATED).body(feedbackDto);
  }

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<FeedbackDto> get(@PathVariable Long ticketId) {
    return ResponseEntity.ok(feedbackService.getDtoByTicketId(ticketId));
  }
}
