package by.training.novikov.feedback.exception;

public class FeedbackException extends RuntimeException {

    public FeedbackException(String message) {
        super(message);
    }
}
