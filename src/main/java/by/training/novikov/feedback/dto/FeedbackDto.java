package by.training.novikov.feedback.dto;

import by.training.novikov.user.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class FeedbackDto implements Serializable {

  private Long id;
  private UserDto user;
  private String text;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
  private Date date;

  @Min(1)
  @Max(5)
  @NotNull
  private Integer rate;

  public FeedbackDto() {

  }

  public static FeedbackDto.Builder newBuilder() {
    return new FeedbackDto().new Builder();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(UserDto user) {
    this.user = user;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Integer getRate() {
    return rate;
  }

  public void setRate(Integer rate) {
    this.rate = rate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    FeedbackDto that = (FeedbackDto) o;
    return Objects.equals(user, that.user) && Objects.equals(text, that.text) && Objects.equals(date, that.date) && Objects.equals(rate, that.rate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user, text, date, rate);
  }

  public class Builder {

    private Builder() {

    }

    public Builder setId(Long id) {
      FeedbackDto.this.id = id;

      return this;
    }

    public Builder setUser(UserDto user) {
      FeedbackDto.this.user = user;

      return this;
    }

    public Builder setRate(Integer rate) {
      FeedbackDto.this.rate = rate;

      return this;
    }

    public Builder setText(String text) {
      FeedbackDto.this.text = text;

      return this;
    }

    public Builder setDate(Date date) {
      FeedbackDto.this.date = date;

      return this;
    }

    public FeedbackDto build() {
      return FeedbackDto.this;
    }
  }
}
