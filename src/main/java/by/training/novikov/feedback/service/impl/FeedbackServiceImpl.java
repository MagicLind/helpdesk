package by.training.novikov.feedback.service.impl;

import by.training.novikov.common.exception.NotFoundException;
import by.training.novikov.email.service.EmailService;
import by.training.novikov.feedback.converter.FeedbackConverter;
import by.training.novikov.feedback.domain.Feedback;
import by.training.novikov.feedback.dto.FeedbackDto;
import by.training.novikov.feedback.exception.FeedbackException;
import by.training.novikov.feedback.repository.FeedbackRepository;
import by.training.novikov.feedback.service.FeedbackService;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.ticket.exception.TicketForbiddenException;
import by.training.novikov.ticket.service.TicketService;
import by.training.novikov.user.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FeedbackServiceImpl implements FeedbackService {

  private final FeedbackRepository feedbackRepository;
  private final FeedbackConverter feedbackConverter;
  private final TicketService ticketService;
  private final EmailService emailService;
  private final UserService userService;

  public FeedbackServiceImpl(
    FeedbackRepository feedbackRepository,
    FeedbackConverter feedbackConverter,
    TicketService ticketService,
    EmailService emailService, UserService userService) {
    this.feedbackRepository = feedbackRepository;
    this.feedbackConverter = feedbackConverter;
    this.ticketService = ticketService;
    this.emailService = emailService;
    this.userService = userService;
  }

  @Override
  @Transactional
  public FeedbackDto create(FeedbackDto feedbackDto, Long ticketId) {
    Ticket ticketOwner = ticketService.getEntity(ticketId);
    checkFeedbackOnException(ticketOwner);
    Feedback feedback = feedbackConverter.toEntity(feedbackDto, ticketOwner, userService.getCurrentUser());
    feedback.setTicket(ticketOwner);
    emailService.sendMessageFeedbackProvided(ticketOwner);

    return feedbackConverter.fromEntity(feedbackRepository.save(feedback));
  }

  @Override
  @Transactional
  public FeedbackDto getDtoByTicketId(Long ticketId) {
    ticketService.checkCredential(ticketId);
    Feedback feedback = feedbackRepository.findByTicketId(ticketId)
      .orElseThrow(() -> new NotFoundException("Feedback with specified ticket id:" + ticketId + " not found"));

    return feedbackConverter.fromEntity(feedback);
  }

  private void checkFeedbackOnException(Ticket ticket){
    checkTicketIsDone(ticket);
    checkFeedbackIsNotCreated(ticket);
    checkUserIsOwner(ticket);
  }

  private void checkFeedbackIsNotCreated(Ticket ticket) {
    if(feedbackRepository.findByTicketId(ticket.getId()).isPresent()) {
      throw new FeedbackException("Feedback already is created");
    }
  }

  private void checkTicketIsDone(Ticket ticket) {
    if(!ticket.getState().equals(State.DONE)){
      throw new FeedbackException("Ticket have not state DONE");
    }
  }

  private void checkUserIsOwner(Ticket ticket) {
    if (!ticket.getOwner().equals(userService.getCurrentUser())) {
      throw new TicketForbiddenException("User is not owner of the ticket");
    }
  }
}
