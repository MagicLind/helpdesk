package by.training.novikov.feedback.service;

import by.training.novikov.feedback.dto.FeedbackDto;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
public interface FeedbackService {

  FeedbackDto create(FeedbackDto feedbackDto, @NotNull(message = "Tickets id should not be null") Long ticketId);

  FeedbackDto getDtoByTicketId(@NotNull(message = "Tickets id should not be null") Long ticketId);
}
