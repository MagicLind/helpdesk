package by.training.novikov.common.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("unchecked")
public abstract class AbstractGenericDao<E extends Serializable> {

  public static final String TICKET = "ticket";
  private final Class<E> entityClass;

  private final SessionFactory sessionFactory;

  protected AbstractGenericDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
    this.entityClass =
        (Class<E>)
            ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
  }

  protected Session getSession() {
    return this.sessionFactory.getCurrentSession();
  }

  public E save(E entity) {
    getSession().save(entity);

    return entity;
  }

  public void delete(E entity) {
    getSession().delete(entity);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public E update(E entity) {
    getSession().update(entity);

    return entity;
  }

  public List<E> getAll() {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<E> criteriaQuery = builder.createQuery(entityClass);
    Root<E> root = criteriaQuery.from(entityClass);
    criteriaQuery.select(root);
    Query<E> query = getSession().createQuery(criteriaQuery);

    return query.getResultList();
  }

  protected Optional<E> findByParameter(Serializable value, String... parameter) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<E> criteriaQuery = builder.createQuery(entityClass);
    Root<E> root = criteriaQuery.from(entityClass);
    Path<E> path = getPath(root, parameter);

    criteriaQuery.select(root).where(builder.equal(path, value));
    Query<E> query = getSession().createQuery(criteriaQuery);

    return query.uniqueResultOptional();
  }

  protected List<E> findAllByParameter(Serializable value, String... parameter) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<E> criteriaQuery = builder.createQuery(entityClass);
    Root<E> root = criteriaQuery.from(entityClass);
    Path<E> path = getPath(root, parameter);

    criteriaQuery.select(root).where(builder.equal(path, value));
    Query<E> query = getSession().createQuery(criteriaQuery);

    return query.getResultList();
  }

  protected List<E> findAllByParameterWithLimit(Serializable value, Integer limit, String... parameter) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<E> criteriaQuery = builder.createQuery(entityClass);
    Root<E> root = criteriaQuery.from(entityClass);
    Path<E> path = getPath(root, parameter);

    criteriaQuery.select(root).where(builder.equal(path, value));
    Query<E> query = getSession().createQuery(criteriaQuery).setMaxResults(limit);

    return query.getResultList();
  }

  private Path<E> getPath(Root<E> root, String[] parameter) {
    Path<E> path = root;
    for (String s : parameter) {
      path = path.get(s);
    }

    return path;
  }
}
