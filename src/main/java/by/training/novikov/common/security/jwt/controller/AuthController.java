package by.training.novikov.common.security.jwt.controller;

import by.training.novikov.common.security.CustomUserPrincipal;
import by.training.novikov.common.security.jwt.JwtUtils;
import by.training.novikov.common.security.jwt.payload.request.LoginRequest;
import by.training.novikov.common.security.jwt.payload.response.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  JwtUtils jwtUtils;

  @PostMapping("/signin")
  public ResponseEntity<JwtResponse> authenticateUser(
    @Valid @RequestBody LoginRequest loginRequest) {

    Authentication authentication =
      authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
          loginRequest.getEmail(), loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);

    CustomUserPrincipal userDetails = (CustomUserPrincipal) authentication.getPrincipal();
    JwtResponse jwtResponse = new JwtResponse(
      userDetails.getId(), jwt, userDetails.getUsername(), userDetails.getRole());

    return ResponseEntity.status(HttpStatus.OK)
      .header("token", jwt)
      .header("tokenType", jwtResponse.getType())
      .body(jwtResponse);
  }
}
