package by.training.novikov.common.security.service;

import by.training.novikov.common.security.CustomUserPrincipal;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository  repository) {
        this.userRepository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(
            () -> new UsernameNotFoundException("User with email='" + email + "' not found"));
        return new CustomUserPrincipal(user);
    }
}
