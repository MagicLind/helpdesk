package by.training.novikov.common.security;

import by.training.novikov.common.security.handler.AuthEntryPointJwt;
import by.training.novikov.common.security.handler.CustomAccessDeniedHandler;
import by.training.novikov.common.security.jwt.AuthTokenFilter;
import by.training.novikov.common.security.service.CustomUserDetailsService;
import by.training.novikov.user.domain.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private static final String COOKIES = "JSESSIONID";
  private static final int MAXIMUM_SESSIONS_COUNT = 1;
  private static final String[] URLS_GET_TICKET = {
    "/api/v1/tickets/**", "/api/v1/tickets", "/api/v1/tickets/my"
  };
  private static final String[] URLS_CREATE_TICKET = {
    "/api/v1/tickets/submit", "/api/v1/tickets/save"
  };
  private static final String URL_ATTACHMENT = "/api/v1/tickets/**/attachments/**";

  private static final String[] ALL_ROLES = {
    Role.EMPLOYEE.name(), Role.ENGINEER.name(), Role.MANAGER.name()
  };
  private static final String[] ROLES_OF_TICKET_CREATORS = {
    Role.EMPLOYEE.name(), Role.MANAGER.name()
  };

  private final CustomUserDetailsService userDetailsService;
  private final PasswordEncoder encoder;
  private final AuthEntryPointJwt unauthorizedHandler;
  private final CustomAccessDeniedHandler accessDeniedHandler;

  @Autowired
  public SecurityConfig(
      CustomUserDetailsService userDetailsService,
      PasswordEncoder encoder,
      AuthEntryPointJwt unauthorizedHandler,
      CustomAccessDeniedHandler accessDeniedHandler) {
    this.userDetailsService = userDetailsService;
    this.encoder = encoder;
    this.unauthorizedHandler = unauthorizedHandler;
    this.accessDeniedHandler = accessDeniedHandler;
  }

  @Bean
  public AuthTokenFilter authenticationJwtTokenFilter() {
    return new AuthTokenFilter();
  }

  @Override
  protected void configure(final AuthenticationManagerBuilder authenticationManagerBuilder)
      throws Exception {
    authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(encoder);
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
    CorsConfiguration configuration = new CorsConfiguration();
    configuration.setAllowedOrigins(Collections.singletonList("*"));
    configuration.setAllowedMethods(Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"));
    configuration.setAllowedHeaders(
        Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
    configuration.setExposedHeaders(Collections.singletonList("Content-Disposition"));
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);
    return source;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http.cors()

        .and()
        .csrf()
        .disable()
        .authorizeRequests()

        .antMatchers("/auth/signin")
        .permitAll()

        .antMatchers(
            HttpMethod.GET,
            Arrays.toString(URLS_GET_TICKET),
            URL_ATTACHMENT,
            "/api/v1/categories",
            "/api/v1/tickets/**/feedbacks/**",
            "/api/v1/tickets/**/comments/**",
            "/api/v1/tickets/**/histories/**")
        .hasAnyAuthority(ALL_ROLES)

        .antMatchers(
            HttpMethod.POST,
            Arrays.toString(URLS_CREATE_TICKET),
            URL_ATTACHMENT,
            "/api/v1/tickets/**/feedbacks/**")
        .hasAnyAuthority(ROLES_OF_TICKET_CREATORS)

        .antMatchers(HttpMethod.DELETE, URL_ATTACHMENT)
        .hasAnyAuthority(ROLES_OF_TICKET_CREATORS)

        .antMatchers(HttpMethod.POST, "/api/v1/tickets/**/comments/**")
        .hasAnyAuthority(ALL_ROLES)

        .antMatchers(HttpMethod.PUT, "/api/v1/tickets/**/edit")
        .hasAnyAuthority(ROLES_OF_TICKET_CREATORS)

        .antMatchers(HttpMethod.PUT, "/api/v1/tickets/**/cancel")
        .hasAnyAuthority(ALL_ROLES)

        .antMatchers(HttpMethod.PUT, "/api/v1/tickets/**/approve", "/api/v1/tickets/**/decline")
        .hasAnyAuthority(Role.MANAGER.name())

        .antMatchers(HttpMethod.PUT, "/api/v1/tickets/**/assign", "/api/v1/tickets/**/done")
        .hasAnyAuthority(Role.ENGINEER.name())

        .antMatchers("/")
        .permitAll()

        .antMatchers("/h2-console/**")
        .permitAll()

        .and()
        .httpBasic()
        .authenticationEntryPoint(unauthorizedHandler)

        .and()
        .exceptionHandling()
        .accessDeniedHandler(accessDeniedHandler)

        .and()
        .logout()
        .invalidateHttpSession(true)
        .deleteCookies(COOKIES)
        .permitAll()

        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .sessionFixation()
        .changeSessionId()
        .maximumSessions(MAXIMUM_SESSIONS_COUNT);

    http.headers().frameOptions().disable();
    http.addFilterBefore(
        authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
  }
}
