package by.training.novikov.common.security.jwt.payload.response;

public class JwtResponse {

  private String type = "Bearer";
  private Long id;
  private String email;
  private String role;
  private String token;

  public JwtResponse(Long id, String token, String email, String role) {
    this.id = id;
    this.token = token;
    this.email = email;
    this.role = role;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
