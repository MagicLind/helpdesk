package by.training.novikov.common.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import java.io.IOException;
import java.util.Collections;
import java.util.Objects;
import java.util.Properties;

@Configuration
@PropertySource("classpath:mail/emailconfig.properties")
public class SpringMailConfig implements ApplicationContextAware, EnvironmentAware {

  private static final String ENCODING = "UTF-8";
  private static final String JAVA_MAIL_FILE = "classpath:mail/javamail.properties";

  @Value("mail.server.host")
  private String host ;
  @Value("mail.server.port")
  private String port;
  @Value("mail.server.protocol")
  private String protocol;
  @Value("mail.server.username")
  private String username;
  @Value("mail.server.password")
  private String password;

  private ApplicationContext applicationContext;
  private Environment environment;

  @Override
  public void setApplicationContext(@NotNull final ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  @Override
  public void setEnvironment(@NotNull final Environment environment) {
    this.environment = environment;
  }

  @Bean
  public JavaMailSender mailSender() throws IOException {
    final JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    mailSender.setHost(this.environment.getProperty(host));
    mailSender.setPort(
        Integer.parseInt(Objects.requireNonNull(this.environment.getProperty(port))));
    mailSender.setProtocol(this.environment.getProperty(protocol));
    mailSender.setUsername(this.environment.getProperty(username));
    mailSender.setPassword(this.environment.getProperty(password));

    final Properties javaMailProperties = new Properties();
    javaMailProperties.load(this.applicationContext.getResource(JAVA_MAIL_FILE).getInputStream());
    mailSender.setJavaMailProperties(javaMailProperties);

    return mailSender;
  }

  @Bean
  public ReloadableResourceBundleMessageSource messageSource() {
    ReloadableResourceBundleMessageSource messageSource =
        new ReloadableResourceBundleMessageSource();
    messageSource.setBasenames("classpath:mail/messages/messages");
    messageSource.setDefaultEncoding(ENCODING);
    return messageSource;
  }

  @Bean
  public TemplateEngine emailTemplateEngine() {
    final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
    templateEngine.addTemplateResolver(htmlTemplateResolver());
    templateEngine.addTemplateResolver(stringTemplateResolver());
    return templateEngine;
  }

  private ITemplateResolver htmlTemplateResolver() {
    final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
    templateResolver.setOrder(2);
    templateResolver.setResolvablePatterns(Collections.singleton("html/*"));
    templateResolver.setPrefix("/mail/");
    templateResolver.setSuffix(".html");
    templateResolver.setTemplateMode(TemplateMode.HTML);
    templateResolver.setCharacterEncoding(ENCODING);
    templateResolver.setCacheable(false);
    return templateResolver;
  }

  private ITemplateResolver stringTemplateResolver() {
    final StringTemplateResolver templateResolver = new StringTemplateResolver();
    templateResolver.setOrder(3);
    templateResolver.setTemplateMode("HTML5");
    templateResolver.setCacheable(false);
    return templateResolver;
  }
}
