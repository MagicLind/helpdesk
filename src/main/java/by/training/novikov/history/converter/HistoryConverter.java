package by.training.novikov.history.converter;

import by.training.novikov.history.domain.History;
import by.training.novikov.history.dto.HistoryDto;
import by.training.novikov.user.converter.UserConverter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class HistoryConverter {

  private final UserConverter userConverter;

  public HistoryConverter(UserConverter userConverter) {
    this.userConverter = userConverter;
  }

  public HistoryDto fromEntity(History entity) {
    return HistoryDto.newBuilder()
      .setAction(entity.getAction())
      .setDate(entity.getDate())
      .setDescription(entity.getDescription())
      .setUser(userConverter.fromEntity(entity.getUser()))
      .build();
  }

  public List<HistoryDto> fromListEntity(List<History> histories) {
    return histories.stream().map(this::fromEntity).collect(Collectors.toList());
  }
}
