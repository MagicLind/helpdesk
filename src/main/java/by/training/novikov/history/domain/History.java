package by.training.novikov.history.domain;

import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.user.domain.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
public class History implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(nullable = false)
  private Ticket ticket;

  @OneToOne
  @JoinColumn(nullable = false)
  private User user;

  @Column(nullable = false)
  private Date date;

  @Column(nullable = false)
  private String action;

  @Column(nullable = false)
  private String description;

  public History() {
  }

  public static History.Builder newBuilder() {
    return new History().new Builder();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Ticket getTicket() {
    return ticket;
  }

  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    History history = (History) o;
    return Objects.equals(ticket, history.ticket) && Objects.equals(user, history.user) && Objects.equals(date, history.date) && Objects.equals(action, history.action) && Objects.equals(description, history.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ticket, user, date, action, description);
  }

  public class Builder {

    private Builder() {

    }

    public Builder setTicket(Ticket ticket) {
      History.this.ticket = ticket;

      return this;
    }

    public Builder setDate(Date date) {
      History.this.date = date;

      return this;
    }

    public Builder setAction(String action) {
      History.this.action = action;

      return this;
    }

    public Builder setUser(User user) {
      History.this.user = user;

      return this;
    }

    public Builder setDescription(String description) {
      History.this.description = description;

      return this;
    }

    public History build() {
      return History.this;
    }
  }
}



