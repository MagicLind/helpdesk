package by.training.novikov.history.service.impl;

import by.training.novikov.history.converter.HistoryConverter;
import by.training.novikov.history.domain.History;
import by.training.novikov.history.dto.HistoryDto;
import by.training.novikov.history.repository.HistoryRepository;
import by.training.novikov.history.service.HistoryService;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.user.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class HistoryServiceImpl implements HistoryService {

  private static final Integer DEFAULT_COUNT_HISTORIES = 5;

  private final HistoryRepository historyRepository;
  private final UserService userService;
  private final HistoryConverter historyConverter;

  public HistoryServiceImpl(
    HistoryRepository historyRepository,
    UserService userService,
    HistoryConverter historyConverter) {
    this.historyRepository = historyRepository;
    this.userService = userService;
    this.historyConverter = historyConverter;
  }

  @Override
  @Transactional
  public List<HistoryDto> getAll(Long ticketId) {
    return historyConverter.fromListEntity(historyRepository.getAllByTicketId(ticketId));
  }

  @Override
  @Transactional
  public List<HistoryDto> getDefault(Long ticketId) {
    return historyConverter.fromListEntity(historyRepository.getAllByTicketIdWithLimit(ticketId, DEFAULT_COUNT_HISTORIES));
  }

  @Override
  @Transactional
  public void addHistoryFileRemoved(String fileName, Ticket ticket) {
    History history = History.newBuilder()
      .setTicket(ticket)
      .setDate(Date.from(Calendar.getInstance().toInstant()))
      .setUser(userService.getCurrentUser())
      .setAction("File is removed")
      .setDescription(String.format("File is removed:%s", fileName))
      .build();

    historyRepository.save(history);
  }

  @Override
  @Transactional
  public void addHistoryFileAttached(String fileName, Ticket ticket) {
    History history = History.newBuilder()
      .setTicket(ticket)
      .setDate(Date.from(Calendar.getInstance().toInstant()))
      .setUser(userService.getCurrentUser())
      .setAction("File is attached")
      .setDescription(String.format("File is attached:%s", fileName))
      .build();

    historyRepository.save(history);
  }

  @Override
  @Transactional
  public void addHistoryTicketStatusChanged(State oldState, State newState, Ticket ticket) {
    History history = History.newBuilder()
      .setTicket(ticket)
      .setDate(Date.from(Calendar.getInstance().toInstant()))
      .setUser(userService.getCurrentUser())
      .setAction("Ticket Status is changed")
      .setDescription(String.format("Ticket Status is changed from %s to %s", oldState, newState))
      .build();

    historyRepository.save(history);
  }

  @Override
  @Transactional
  public void addHistory(String action, Ticket ticket) {
    History history = History.newBuilder()
      .setTicket(ticket)
      .setDate(Date.from(Calendar.getInstance().toInstant()))
      .setUser(userService.getCurrentUser())
      .setAction(action)
      .setDescription(action)
      .build();

    historyRepository.save(history);
  }
}
