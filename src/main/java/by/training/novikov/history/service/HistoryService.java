package by.training.novikov.history.service;

import by.training.novikov.history.dto.HistoryDto;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;

import java.util.List;

public interface HistoryService {
  List<HistoryDto> getAll(Long ticketId);

  List<HistoryDto> getDefault(Long ticketId);

  void addHistoryFileRemoved(String fileName, Ticket ticket);

  void addHistoryFileAttached(String fileName, Ticket ticket);

  void addHistoryTicketStatusChanged(State oldState, State newState, Ticket ticket);

  void addHistory(String action, Ticket ticket);
}
