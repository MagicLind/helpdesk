package by.training.novikov.history.repository;

import by.training.novikov.history.domain.History;

import java.util.List;

public interface HistoryRepository {

  List<History> getAllByTicketId(Long ticketId);

  List<History> getAllByTicketIdWithLimit(Long ticketId, Integer limit);

  History save(History history);
}
