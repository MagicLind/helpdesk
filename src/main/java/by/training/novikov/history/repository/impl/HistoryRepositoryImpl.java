package by.training.novikov.history.repository.impl;

import by.training.novikov.common.dao.AbstractGenericDao;
import by.training.novikov.history.domain.History;
import by.training.novikov.history.repository.HistoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HistoryRepositoryImpl extends AbstractGenericDao<History> implements HistoryRepository {

  protected HistoryRepositoryImpl(SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  @Override
  public List<History> getAllByTicketIdWithLimit(Long ticketId, Integer limit) {
    return findAllByParameterWithLimit(ticketId, limit, "ticket", "id");
  }

  @Override
  public List<History> getAllByTicketId(Long ticketId) {
    return findAllByParameter(ticketId, "ticket", "id");
  }
}
