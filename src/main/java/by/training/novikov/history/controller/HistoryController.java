package by.training.novikov.history.controller;

import by.training.novikov.history.dto.HistoryDto;
import by.training.novikov.history.service.HistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1/tickets/{ticketId}/histories")
public class HistoryController {

  private final HistoryService historyService;

  public HistoryController(HistoryService historyService) {
    this.historyService = historyService;
  }

  @GetMapping(value = "/all", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<HistoryDto>> getAll(@PathVariable Long ticketId) {
    return ResponseEntity.ok(historyService.getAll(ticketId));
  }

  @GetMapping(value = "/default", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<HistoryDto>> getDefault(@PathVariable Long ticketId) {
    return ResponseEntity.ok(historyService.getDefault(ticketId));
  }
}
