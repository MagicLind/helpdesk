package by.training.novikov.history.dto;

import by.training.novikov.user.dto.UserDto;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class HistoryDto implements Serializable {

  private UserDto user;
  private String action;
  private String description;
  private Date date;

  public HistoryDto() {
  }

  public static HistoryDto.Builder newBuilder() {
    return new HistoryDto().new Builder();
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public UserDto getUser() {
    return user;
  }

  public void setUser(UserDto user) {
    this.user = user;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    HistoryDto that = (HistoryDto) o;
    return Objects.equals(date, that.date) && Objects.equals(action, that.action) && Objects.equals(user, that.user) && Objects.equals(description, that.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, action, user, description);
  }


  public class Builder {

    private Builder() {

    }

    public Builder setDate(Date date) {
      HistoryDto.this.date = date;

      return this;
    }

    public Builder setAction(String action) {
      HistoryDto.this.action = action;

      return this;
    }

    public Builder setUser(UserDto user) {
      HistoryDto.this.user = user;

      return this;
    }

    public Builder setDescription(String description) {
      HistoryDto.this.description = description;

      return this;
    }

    public HistoryDto build() {
      return HistoryDto.this;
    }
  }
}


