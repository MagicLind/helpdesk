package by.training.novikov.category.repository;

import by.training.novikov.category.domain.Category;

import java.util.List;

public interface CategoryRepository {

    List<Category> getAll();
}
