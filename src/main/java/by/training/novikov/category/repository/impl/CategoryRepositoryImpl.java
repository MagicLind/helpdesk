package by.training.novikov.category.repository.impl;

import by.training.novikov.category.domain.Category;
import by.training.novikov.category.repository.CategoryRepository;
import by.training.novikov.common.dao.AbstractGenericDao;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepositoryImpl extends AbstractGenericDao<Category> implements CategoryRepository {

  protected CategoryRepositoryImpl(SessionFactory sessionFactory) {
    super(sessionFactory);
  }
}
