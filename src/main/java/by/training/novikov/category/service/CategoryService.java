package by.training.novikov.category.service;


import by.training.novikov.category.domain.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();
}
