package by.training.novikov.category.service.impl;

import by.training.novikov.category.domain.Category;
import by.training.novikov.category.repository.CategoryRepository;
import by.training.novikov.category.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    @Transactional
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }
}
