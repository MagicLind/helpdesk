package by.training.novikov.ticket.dto.validator.date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Calendar;
import java.util.Date;


public class DateConstraintValidator implements ConstraintValidator<NotLessCurrentDate, Date> {

  @Override
  public void initialize(NotLessCurrentDate notLessCurrentDate) {}

  @Override
  public boolean isValid(Date date, ConstraintValidatorContext cxt) {
    return validateFile(date);
  }

  private boolean validateFile(Date date) {
    Date currentDate = Date.from(Calendar.getInstance().toInstant());

    if(date == null){
      return true;
    }

    return date.after(currentDate) || date.equals(currentDate);
  }
}
