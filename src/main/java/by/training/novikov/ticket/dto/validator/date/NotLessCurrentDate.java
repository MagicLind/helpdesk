package by.training.novikov.ticket.dto.validator.date;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateConstraintValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NotLessCurrentDate {

    String message() default "Date less then current date or null";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}