package by.training.novikov.ticket.dto;

import by.training.novikov.category.domain.Category;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.ticket.domain.enums.Urgency;
import by.training.novikov.ticket.dto.validator.date.NotLessCurrentDate;
import by.training.novikov.user.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TicketDto implements Serializable {

  private Long id;

  @NotBlank(message = "Name can't be blank or null")
  @Pattern(
      regexp = "[a-z0-9~.\"(),:;<>@\\[\\]!#$%&'*+-/=?^_`{|} ]*",
      message =
          "Name consist of lowercase English alpha characters,"
              + " digits and special characters")
  @Size(max = 100, message = "The max size of name is 100 chapters")
  private String name;

  @Pattern(
      regexp = "[a-zA-Z0-9~.\"(),:;<>@\\[\\]!#$%&'*+-/=?^_`{|} ]*",
      message =
          "Description consist of lowercase and uppercase English alpha characters,"
              + " digits and special characters")
  @Size(max = 500, message = "The max size of description is 500 chapters")
  private String description;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
  private Date createdOn;

  @NotLessCurrentDate
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
  private Date desiredResolutionDate;

  @NotNull(message = "Urgency can't be null")
  private Urgency urgency;

  @NotNull(message = "Category can't be null")
  private Category category;

  private State state;
  private UserDto assignee;
  private UserDto owner;
  private UserDto approver;

  public TicketDto() {
  }

  public static TicketDto.Builder newBuilder() {
    return new TicketDto().new Builder();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public Date getDesiredResolutionDate() {
    return desiredResolutionDate;
  }

  public void setDesiredResolutionDate(Date desiredResolutionDate) {
    this.desiredResolutionDate = desiredResolutionDate;
  }

  public Urgency getUrgency() {
    return urgency;
  }

  public void setUrgency(Urgency urgency) {
    this.urgency = urgency;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public UserDto getAssignee() {
    return assignee;
  }

  public void setAssignee(UserDto assignee) {
    this.assignee = assignee;
  }

  public UserDto getOwner() {
    return owner;
  }

  public void setOwner(UserDto owner) {
    this.owner = owner;
  }

  public UserDto getApprover() {
    return approver;
  }

  public void setApprover(UserDto approver) {
    this.approver = approver;
  }

  public Category getCategory() {
    return category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TicketDto ticketDto = (TicketDto) o;
    return Objects.equals(name, ticketDto.name)
        && Objects.equals(description, ticketDto.description)
        && Objects.equals(createdOn, ticketDto.createdOn)
        && Objects.equals(desiredResolutionDate, ticketDto.desiredResolutionDate)
        && urgency == ticketDto.urgency
        && Objects.equals(category, ticketDto.category) && state == ticketDto.state && Objects.equals(assignee, ticketDto.assignee) && Objects.equals(owner, ticketDto.owner) && Objects.equals(approver, ticketDto.approver);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name,
        description,
        createdOn, desiredResolutionDate, urgency, category, state, assignee, owner, approver);
  }


  public class Builder {

    private Builder() {

    }

    public Builder setId(Long id) {
      TicketDto.this.id = id;

      return this;
    }

    public Builder setName(String name) {
      TicketDto.this.name = name;

      return this;
    }

    public Builder setDescription(String description) {
      TicketDto.this.description = description;

      return this;
    }

    public Builder setCreatedOn(Date createdOn) {
      TicketDto.this.createdOn = createdOn;

      return this;
    }

    public Builder setCategory(Category category) {
      TicketDto.this.category = category;

      return this;
    }

    public Builder setDesiredResolutionDate(Date desiredResolutionDate) {
      TicketDto.this.desiredResolutionDate = desiredResolutionDate;

      return this;
    }

    public Builder setUrgency(Urgency urgency) {
      TicketDto.this.urgency = urgency;

      return this;
    }

    public Builder setState(State state) {
      TicketDto.this.state = state;

      return this;
    }

    public Builder setOwner(UserDto owner) {
      TicketDto.this.owner = owner;

      return this;
    }

    public Builder setAssignee(UserDto assignee) {
      TicketDto.this.assignee = assignee;

      return this;
    }

    public Builder setApprover(UserDto approver) {
      TicketDto.this.approver = approver;

      return this;
    }

    public TicketDto build() {
      return TicketDto.this;
    }
  }
}
