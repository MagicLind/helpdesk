package by.training.novikov.ticket.converter;

import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.dto.TicketDto;
import by.training.novikov.user.converter.UserConverter;
import by.training.novikov.user.domain.User;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TicketConverter {

  private final UserConverter userConverter;

  public TicketConverter(UserConverter userConverter) {
    this.userConverter = userConverter;
  }

  public List<TicketDto> fromListEntity(List<Ticket> tickets) {
    return tickets.stream().map(this::fromEntity).collect(Collectors.toList());
  }

  public TicketDto fromEntity(Ticket entity) {
    return TicketDto.newBuilder()
      .setId(entity.getId())
      .setName(entity.getName())
      .setDescription(entity.getDescription())
      .setCreatedOn(entity.getCreatedOn())
      .setDesiredResolutionDate(entity.getDesiredResolutionDate())
      .setUrgency(entity.getUrgency())
      .setState(entity.getState())
      .setCategory(entity.getCategory())
      .setOwner(userConverter.fromEntity(entity.getOwner()))
      .setApprover(entity.getApprover() == null ? null : userConverter.fromEntity(entity.getApprover()))
      .setAssignee(entity.getAssignee() == null ? null : userConverter.fromEntity(entity.getAssignee()))
      .build();
  }

  public Ticket toEntity(TicketDto dto, User owner) {
    return Ticket.newBuilder()
      .setId(dto.getId())
      .setName(dto.getName())
      .setDescription(dto.getDescription())
      .setCreatedOn(Date.from(Calendar.getInstance().toInstant()))
      .setDesiredResolutionDate(dto.getDesiredResolutionDate())
      .setUrgency(dto.getUrgency())
      .setState(dto.getState())
      .setCategory(dto.getCategory())
      .setOwner(owner)
      .build();
  }
}
