package by.training.novikov.ticket.exception;

public class TicketForbiddenException extends RuntimeException {

  public TicketForbiddenException(String message) {
    super(message);
  }
}
