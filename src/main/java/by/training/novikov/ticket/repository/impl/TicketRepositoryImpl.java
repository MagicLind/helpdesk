package by.training.novikov.ticket.repository.impl;

import by.training.novikov.common.dao.AbstractGenericDao;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.ticket.repository.TicketRepository;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class TicketRepositoryImpl extends AbstractGenericDao<Ticket> implements TicketRepository {

  public static final String STATE = "state";
  public static final String OWNER = "owner";

  protected TicketRepositoryImpl(SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  @Override
  public Optional<Ticket> findById(Long id) {
    return findByParameter(id,"id");
  }

  @Override
  public List<Ticket> findAllByOwnerId(Long userId) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
    Root<Ticket> ticketRoot = criteriaQuery.from(Ticket.class);
    criteriaQuery.select(ticketRoot)
      .where(builder.equal(ticketRoot.get(OWNER).get("id"), userId));
    Query<Ticket> query = getSession().createQuery(criteriaQuery);

    return query.getResultList();
  }

  @Override
  public List<Ticket> findAllByOwnerIdOrApproverIdAndByState(Long userId, State state) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
    Root<Ticket> ticketRoot = criteriaQuery.from(Ticket.class);
    criteriaQuery.select(ticketRoot)
      .where(builder.or(builder.equal(ticketRoot.get(OWNER).get("id"), userId),
        builder.and(builder.equal(ticketRoot.get("approver").get("id"), userId)
          , builder.equal(ticketRoot.get(STATE), state))));
    Query<Ticket> query = getSession().createQuery(criteriaQuery);

    return query.getResultList();
  }

  @Override
  public List<Ticket> findAllByAssigneeId(Long userId) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
    Root<Ticket> ticketRoot = criteriaQuery.from(Ticket.class);
    criteriaQuery.select(ticketRoot)
      .where(builder.equal(ticketRoot.get("assignee").get("id"), userId));
    Query<Ticket> query = getSession().createQuery(criteriaQuery);

    return query.getResultList();
  }

  @Override
  public List<Ticket> findAllByOwnerIdOrStateOrApproverIdAndInStates(Long userId, State state, Set<State> states) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
    Root<Ticket> ticketRoot = criteriaQuery.from(Ticket.class);
    criteriaQuery.select(ticketRoot)
      .where(builder.or(builder.equal(ticketRoot.get(OWNER).get("id"), userId),
        builder.equal(ticketRoot.get(STATE), state),
        builder.and(builder.equal(ticketRoot.get("approver").get("id"), userId),
          ticketRoot.get(STATE).in(states))));
    Query<Ticket> query = getSession().createQuery(criteriaQuery);

    return query.getResultList();
  }

  @Override
  public List<Ticket> findAllByStateOrAssigneeIdAndInStates(State state, Long userId, Set<State> states) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
    Root<Ticket> ticketRoot = criteriaQuery.from(Ticket.class);
    criteriaQuery.select(ticketRoot)
      .where(builder.or(builder.equal(ticketRoot.get(STATE), state),
        builder.and(builder.equal(ticketRoot.get("assignee").get("id"), userId),
          ticketRoot.get(STATE).in(states))));
    Query<Ticket> query = getSession().createQuery(criteriaQuery);

    return query.getResultList();
  }
}
