package by.training.novikov.ticket.repository;

import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface TicketRepository {

  List<Ticket> findAllByOwnerId(Long id);

  Optional<Ticket> findById(Long id);

  List<Ticket> findAllByOwnerIdOrApproverIdAndByState(Long id, State state);

  List<Ticket> findAllByAssigneeId(Long id);

  List<Ticket> findAllByOwnerIdOrStateOrApproverIdAndInStates(Long id, State state, Set<State> states);

  List<Ticket> findAllByStateOrAssigneeIdAndInStates(State approved, Long id, Set<State> states);

  Ticket update(Ticket ticket);

  Ticket save(Ticket ticket);
}
