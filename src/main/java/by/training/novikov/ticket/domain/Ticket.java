package by.training.novikov.ticket.domain;

import by.training.novikov.category.domain.Category;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.ticket.domain.enums.Urgency;
import by.training.novikov.user.domain.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
public class Ticket implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(length = 100, nullable = false)
  private String name;

  @Column(length = 500, nullable = false)
  private String description;

  @Column(name = "created_on", nullable = false)
  private Date createdOn;

  @Column(name = "desired_resolution_date")
  private Date desiredResolutionDate;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private Urgency urgency;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private State state;

  @OneToOne
  @JoinColumn(nullable = false)
  private User owner;

  @OneToOne
  private User assignee;

  @OneToOne
  private User approver;

  @OneToOne
  @JoinColumn(nullable = false)
  private Category category;

  public Ticket() {
  }

  public static Ticket.Builder newBuilder() {
    return new Ticket().new Builder();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public Date getDesiredResolutionDate() {
    return desiredResolutionDate;
  }

  public void setDesiredResolutionDate(Date desiredResolutionDate) {
    this.desiredResolutionDate = desiredResolutionDate;
  }

  public User getAssignee() {
    return assignee;
  }

  public void setAssignee(User assignee) {
    this.assignee = assignee;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public Category getCategory() {
    return category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public Urgency getUrgency() {
    return urgency;
  }

  public void setUrgency(Urgency urgency) {
    this.urgency = urgency;
  }

  public User getApprover() {
    return approver;
  }

  public void setApprover(User approver) {
    this.approver = approver;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Ticket ticket = (Ticket) o;
    return Objects.equals(name, ticket.name) && Objects.equals(description, ticket.description) && Objects.equals(createdOn, ticket.createdOn) && Objects.equals(desiredResolutionDate, ticket.desiredResolutionDate) && urgency == ticket.urgency && state == ticket.state && Objects.equals(assignee, ticket.assignee) && Objects.equals(owner, ticket.owner) && Objects.equals(approver, ticket.approver) && Objects.equals(category, ticket.category);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, description, createdOn, desiredResolutionDate, urgency, state, assignee, owner, approver, category);
  }

  public class Builder {

    private Builder() {

    }

    public Builder setId(Long id) {
      Ticket.this.id = id;

      return this;
    }

    public Builder setName(String name) {
      Ticket.this.name = name;

      return this;
    }

    public Builder setDescription(String description) {
      Ticket.this.description = description;

      return this;
    }

    public Builder setCreatedOn(Date createdOn) {
      Ticket.this.createdOn = createdOn;

      return this;
    }

    public Builder setCategory(Category category) {
      Ticket.this.category = category;

      return this;
    }

    public Builder setDesiredResolutionDate(Date desiredResolutionDate) {
      Ticket.this.desiredResolutionDate = desiredResolutionDate;

      return this;
    }

    public Builder setUrgency(Urgency urgency) {
      Ticket.this.urgency = urgency;

      return this;
    }

    public Builder setState(State state) {
      Ticket.this.state = state;

      return this;
    }

    public Builder setOwner(User owner) {
      Ticket.this.owner = owner;

      return this;
    }

    public Ticket build() {
      return Ticket.this;
    }
  }
}
