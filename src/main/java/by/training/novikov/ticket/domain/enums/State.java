package by.training.novikov.ticket.domain.enums;

public enum State {
  DRAFT,
  NEW,
  APPROVED,
  DECLINED,
  CANCELLED,
  IN_PROGRESS,
  DONE
}
