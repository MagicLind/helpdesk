package by.training.novikov.ticket.domain.enums;

public enum Urgency {
  CRITICAL,
  HIGH,
  MEDIUM,
  LOW
}
