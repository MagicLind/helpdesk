package by.training.novikov.ticket.controller;

import by.training.novikov.ticket.dto.TicketDto;
import by.training.novikov.ticket.service.TicketService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1/tickets")

public class TicketController {

  private final TicketService ticketService;

  public TicketController(TicketService ticketService) {
    this.ticketService = ticketService;
  }

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<TicketDto>> getAll() {
    return ResponseEntity.ok(ticketService.getAllDto());
  }

  @GetMapping(value = "/my", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<TicketDto>> getAllMy() {
    return ResponseEntity.ok(ticketService.getAllMyDto());
  }

  @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<TicketDto> get(@PathVariable Long id) {
    return ResponseEntity.ok(ticketService.getDto(id));
  }

  @PostMapping(value = "/submit", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<TicketDto> submit(@RequestBody @Valid TicketDto ticketDto) {
    ticketDto = ticketService.submit(ticketDto);
    return ResponseEntity.status(CREATED).body(ticketDto);
  }

  @PostMapping(value = "/save", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<TicketDto> save(@RequestBody @Valid TicketDto ticketDto) {
    ticketDto = ticketService.save(ticketDto);
    return ResponseEntity.status(CREATED).body(ticketDto);
  }

  @PutMapping(value = "/{id}/edit", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<TicketDto> edit(
    @RequestBody @Valid TicketDto ticketDto, @PathVariable Long id) {
    ticketDto = ticketService.edit(ticketDto, id);
    return ResponseEntity.status(OK).body(ticketDto);
  }

  @PutMapping(value = "/{id}/cancel", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<TicketDto> cancel(@PathVariable Long id) {
    TicketDto ticketDto = ticketService.cancel(id);
    return ResponseEntity.status(OK).body(ticketDto);
  }

  @PutMapping(value = "/{id}/approve", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<TicketDto> approve(@PathVariable Long id) {
    TicketDto ticketDto = ticketService.approve(id);
    return ResponseEntity.status(OK).body(ticketDto);
  }

  @PutMapping(value = "/{id}/decline", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<TicketDto> decline(@PathVariable Long id) {
    TicketDto ticketDto = ticketService.decline(id);
    return ResponseEntity.status(OK).body(ticketDto);
  }

  @PutMapping(value = "/{id}/assign", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<TicketDto> assign(@PathVariable Long id) {
    TicketDto ticketDto = ticketService.assign(id);
    return ResponseEntity.status(OK).body(ticketDto);
  }

  @PutMapping(value = "/{id}/done", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<TicketDto> done(@PathVariable Long id) {
    TicketDto ticketDto = ticketService.done(id);
    return ResponseEntity.status(OK).body(ticketDto);
  }
}
