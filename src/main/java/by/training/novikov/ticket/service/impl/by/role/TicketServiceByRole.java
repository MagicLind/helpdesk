package by.training.novikov.ticket.service.impl.by.role;

import by.training.novikov.ticket.domain.Ticket;

import java.util.List;

public interface TicketServiceByRole {

    List<Ticket> getAll();

    Ticket get(Long ticketId);

    List<Ticket> getAllMy();

    Boolean isAvailableCredential(Ticket ticket);
}
