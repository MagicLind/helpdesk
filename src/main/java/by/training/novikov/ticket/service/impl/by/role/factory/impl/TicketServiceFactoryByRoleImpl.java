package by.training.novikov.ticket.service.impl.by.role.factory.impl;

import by.training.novikov.ticket.service.impl.by.role.TicketServiceByRole;
import by.training.novikov.ticket.service.impl.by.role.factory.TicketServiceFactoryByRole;
import by.training.novikov.ticket.service.impl.by.role.impl.TicketServiceByEmployee;
import by.training.novikov.ticket.service.impl.by.role.impl.TicketServiceByEngineer;
import by.training.novikov.ticket.service.impl.by.role.impl.TicketServiceByManager;
import by.training.novikov.user.domain.enums.Role;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;

@Component
public class TicketServiceFactoryByRoleImpl implements TicketServiceFactoryByRole {


    private final Map<Role, TicketServiceByRole> ticketServiceByRole;

    public TicketServiceFactoryByRoleImpl(TicketServiceByEmployee ticketServiceByEmployee,
                                          TicketServiceByManager ticketServiceByManager,
                                          TicketServiceByEngineer ticketServiceByEngineer) {
        ticketServiceByRole = new EnumMap<>(Role.class);
        ticketServiceByRole.put(Role.EMPLOYEE, ticketServiceByEmployee);
        ticketServiceByRole.put(Role.MANAGER, ticketServiceByManager);
        ticketServiceByRole.put(Role.ENGINEER, ticketServiceByEngineer);
    }

    @Override
    public TicketServiceByRole getTicketServiceByRole(Role role) {
        return ticketServiceByRole.get(role);
    }
}
