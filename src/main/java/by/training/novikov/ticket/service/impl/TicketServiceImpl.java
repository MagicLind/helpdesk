package by.training.novikov.ticket.service.impl;

import by.training.novikov.common.exception.NotFoundException;
import by.training.novikov.email.service.EmailService;
import by.training.novikov.history.service.HistoryService;
import by.training.novikov.ticket.converter.TicketConverter;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.ticket.dto.TicketDto;
import by.training.novikov.ticket.exception.TicketForbiddenException;
import by.training.novikov.ticket.repository.TicketRepository;
import by.training.novikov.ticket.service.TicketService;
import by.training.novikov.ticket.service.impl.by.role.TicketServiceByRole;
import by.training.novikov.ticket.service.impl.by.role.factory.TicketServiceFactoryByRole;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.domain.enums.Role;
import by.training.novikov.user.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

  public static final String TICKET_IN_UNACCEPTABLE_STATE = "Ticket in unacceptable state";

  private final UserService userService;
  private final TicketRepository ticketRepository;
  private final TicketConverter ticketConverter;
  private final TicketServiceFactoryByRole ticketServiceFactoryByRole;
  private final HistoryService historyService;
  private final EmailService emailService;

  public TicketServiceImpl(UserService userService,
                           TicketRepository ticketRepository,
                           TicketConverter ticketConverter,
                           TicketServiceFactoryByRole ticketServiceFactoryByRole,
                           HistoryService historyService,
                           EmailService emailService) {
    this.userService = userService;
    this.ticketRepository = ticketRepository;
    this.ticketConverter = ticketConverter;
    this.ticketServiceFactoryByRole = ticketServiceFactoryByRole;
    this.historyService = historyService;
    this.emailService = emailService;
  }

  @Override
  @Transactional
  public List<TicketDto> getAllDto() {
    TicketServiceByRole ticketServiceByRole =
      ticketServiceFactoryByRole.getTicketServiceByRole(userService.getCurrentUser().getRole());

    return ticketConverter.fromListEntity(ticketServiceByRole.getAll());
  }

  @Override
  @Transactional
  public List<TicketDto> getAllMyDto() {
    TicketServiceByRole ticketServiceByRole =
      ticketServiceFactoryByRole.getTicketServiceByRole(userService.getCurrentUser().getRole());

    return ticketConverter.fromListEntity(ticketServiceByRole.getAllMy());
  }

  @Override
  @Transactional
  public TicketDto getDto(Long ticketId) {
    TicketServiceByRole ticketServiceByRole =
      ticketServiceFactoryByRole.getTicketServiceByRole(userService.getCurrentUser().getRole());

    return ticketConverter.fromEntity(ticketServiceByRole.get(ticketId));
  }

  @Override
  @Transactional
  public Ticket getEntity(Long ticketId) {
    TicketServiceByRole ticketServiceByRole =
      ticketServiceFactoryByRole.getTicketServiceByRole(userService.getCurrentUser().getRole());

    return ticketServiceByRole.get(ticketId);
  }

  @Override
  @Transactional
  public TicketDto save(TicketDto ticketDto) {
    Ticket ticket = ticketConverter.toEntity(ticketDto, userService.getCurrentUser());
    ticket.setState(State.DRAFT);
    ticketRepository.save(ticket);
    historyService.addHistory("Ticket is created", ticket);

    return ticketConverter.fromEntity(ticket);
  }

  @Override
  @Transactional
  public TicketDto submit(TicketDto ticketDto) {
    Ticket submittedTicket;

    if (ticketDto.getId() == null) {
      submittedTicket = ticketConverter.toEntity(ticketDto, userService.getCurrentUser());
      submittedTicket.setState(State.NEW);
      ticketRepository.save(submittedTicket);
      historyService.addHistory("Ticket is created", submittedTicket);
    } else {
      Ticket originalTicket = get(ticketDto.getId());

      if ((originalTicket.getState().equals(State.DRAFT)
        || originalTicket.getState().equals(State.DECLINED))
        && originalTicket.getOwner() == userService.getCurrentUser()) {
        submittedTicket = ticketConverter.toEntity(ticketDto, userService.getCurrentUser());
        submittedTicket.setState(State.NEW);
        submittedTicket.setCreatedOn(originalTicket.getCreatedOn());
        ticketRepository.update(submittedTicket);
        historyService.addHistoryTicketStatusChanged(
          originalTicket.getState(), submittedTicket.getState(), submittedTicket);
      } else {
        throw new TicketForbiddenException(TICKET_IN_UNACCEPTABLE_STATE);
      }
    }

    emailService.sendMessageTicketCreated(submittedTicket);

    return ticketConverter.fromEntity(submittedTicket);
  }

  @Override
  @Transactional
  public TicketDto edit(TicketDto ticketDto, Long ticketId) {
    Ticket originalTicket = get(ticketId);

    if (originalTicket.getState().equals(State.DRAFT)
      && originalTicket.getOwner() == userService.getCurrentUser()) {
      Ticket editedTicket = ticketConverter.toEntity(ticketDto, userService.getCurrentUser());
      editedTicket.setId(ticketId);
      editedTicket.setCreatedOn(originalTicket.getCreatedOn());
      editedTicket.setState(State.DRAFT);
      ticketRepository.update(editedTicket);
      historyService.addHistory("Ticket is edited", editedTicket);

      return ticketConverter.fromEntity(editedTicket);
    } else {
      throw new TicketForbiddenException(
        "Ticket forbidden for current user or in unacceptable state");
    }
  }

  @Override
  @Transactional
  public TicketDto cancel(Long ticketId) {
    User user = userService.getCurrentUser();
    Ticket ticket = get(ticketId);
    State oldState = ticket.getState();
    State newState = State.CANCELLED;

    if ((user == ticket.getOwner()
      && (ticket.getState().equals(State.DRAFT)
      || ticket.getState().equals(State.NEW)))
      || (user.getRole().equals(Role.MANAGER) && ticket.getState().equals(State.NEW))
      || (user.getRole().equals(Role.ENGINEER) && ticket.getState().equals(State.APPROVED))) {
      ticket.setState(newState);
      TicketDto ticketDto = ticketConverter.fromEntity(ticketRepository.save(ticket));
      historyService.addHistoryTicketStatusChanged(oldState, newState, ticket);
      if (oldState.equals(State.APPROVED)) {
        emailService.sendMessageTicketCanceledByEngineer(ticket);
      } else if (oldState.equals(State.NEW)) {
        emailService.sendMessageTicketCanceledByManager(ticket);
      }

      return ticketDto;
    } else {
      throw new TicketForbiddenException(
        "Ticket forbidden for current user or ticket in unacceptable state");
    }
  }

  @Override
  @Transactional
  public TicketDto approve(Long ticketId) {
    Ticket ticket = get(ticketId);
    State oldState = ticket.getState();
    State newState = State.APPROVED;
    User approver = userService.getCurrentUser();

    if (ticket.getState().equals(State.NEW) && !ticket.getOwner().equals(approver)) {
      ticket.setState(newState);
      ticket.setApprover(approver);
      TicketDto ticketDto = ticketConverter.fromEntity(ticketRepository.save(ticket));
      historyService.addHistoryTicketStatusChanged(oldState, newState, ticket);
      emailService.sendMessageTicketApproved(ticket);

      return ticketDto;
    } else {
      throw new TicketForbiddenException("Ticket in unacceptable state or approver is owner");
    }
  }

  @Override
  @Transactional
  public TicketDto decline(Long ticketId) {
    Ticket ticket = get(ticketId);
    State oldState = ticket.getState();
    State newState = State.DECLINED;

    if (ticket.getState().equals(State.NEW) && !ticket.getOwner().equals(userService.getCurrentUser())) {
      ticket.setState(newState);
      TicketDto ticketDto = ticketConverter.fromEntity(ticketRepository.save(ticket));
      historyService.addHistoryTicketStatusChanged(oldState, newState, ticket);
      emailService.sendMessageTicketDeclined(ticket);

      return ticketDto;
    } else {
      throw new TicketForbiddenException("Ticket in unacceptable state or created by not employee");
    }
  }

  @Override
  @Transactional
  public TicketDto assign(Long ticketId) {
    Ticket ticket = get(ticketId);
    State oldState = ticket.getState();
    State newState = State.IN_PROGRESS;
    User assigner = userService.getCurrentUser();

    if (ticket.getState().equals(State.APPROVED)) {
      ticket.setState(newState);
      ticket.setAssignee(assigner);
      TicketDto ticketDto = ticketConverter.fromEntity(ticketRepository.save(ticket));
      historyService.addHistoryTicketStatusChanged(oldState, newState, ticket);

      return ticketDto;
    } else {
      throw new TicketForbiddenException(TICKET_IN_UNACCEPTABLE_STATE);
    }
  }

  @Override
  @Transactional
  public TicketDto done(Long ticketId) {
    Ticket ticket = get(ticketId);
    State oldState = ticket.getState();
    State newState = State.DONE;

    if (ticket.getState().equals(State.IN_PROGRESS)) {
      ticket.setState(newState);
      TicketDto ticketDto = ticketConverter.fromEntity(ticketRepository.save(ticket));
      historyService.addHistoryTicketStatusChanged(oldState, newState, ticket);
      emailService.sendMessageTicketDone(ticket);

      return ticketDto;
    } else {
      throw new TicketForbiddenException(TICKET_IN_UNACCEPTABLE_STATE);
    }
  }

  @Override
  @Transactional
  public void checkCredential(Long ticketId) {
    TicketServiceByRole ticketServiceByRole =
      ticketServiceFactoryByRole.getTicketServiceByRole(userService.getCurrentUser().getRole());

    if (!ticketServiceByRole.isAvailableCredential(get(ticketId))) {
      throw new TicketForbiddenException("Ticket forbidden for current user ");
    }
  }

  private Ticket get(Long ticketId) {
    return ticketRepository
      .findById(ticketId)
      .orElseThrow(
        () -> new NotFoundException("Ticket with specified id:" + ticketId + " not found"));
  }
}
