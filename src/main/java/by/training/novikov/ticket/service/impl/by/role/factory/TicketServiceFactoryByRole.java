package by.training.novikov.ticket.service.impl.by.role.factory;

import by.training.novikov.ticket.service.impl.by.role.TicketServiceByRole;
import by.training.novikov.user.domain.enums.Role;

public interface TicketServiceFactoryByRole {

    TicketServiceByRole getTicketServiceByRole(Role role);

}
