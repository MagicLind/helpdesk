package by.training.novikov.ticket.service.impl.by.role.impl;

import by.training.novikov.common.exception.NotFoundException;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.ticket.exception.TicketForbiddenException;
import by.training.novikov.ticket.repository.TicketRepository;
import by.training.novikov.ticket.service.impl.by.role.TicketServiceByRole;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.service.UserService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TicketServiceByEngineer implements TicketServiceByRole {

    private static final Set<State> ACCEPTABLE_STATES = new HashSet<>();

    static {
        ACCEPTABLE_STATES.add(State.IN_PROGRESS);
        ACCEPTABLE_STATES.add(State.DONE);
    }

    private final TicketRepository ticketRepository;
    private final UserService userService;

    public TicketServiceByEngineer(TicketRepository ticketRepository, UserService userService) {
        this.ticketRepository = ticketRepository;
        this.userService = userService;
    }

    @Override
    public List<Ticket> getAll() {
        User user = userService.getCurrentUser();
        return ticketRepository.findAllByStateOrAssigneeIdAndInStates(State.APPROVED,
            user.getId(), ACCEPTABLE_STATES);
    }

    @Override
    public Ticket get(Long ticketId) {
        Ticket ticket = ticketRepository.findById(ticketId)
            .orElseThrow(() -> new NotFoundException("Ticket with specified id:" + ticketId + " not found"));

        if (isAvailableCredential(ticket)) {
            return ticket;
        } else {
            throw new TicketForbiddenException("Ticket forbidden for current user");
        }
    }

    @Override
    public List<Ticket> getAllMy() {
        User user = userService.getCurrentUser();
        return ticketRepository.findAllByAssigneeId(user.getId());
    }

    @Override
    public Boolean isAvailableCredential(Ticket ticket) {
        User user = userService.getCurrentUser();
        return ticket.getState() == State.APPROVED
                || (user.getId().equals(ticket.getAssignee().getId())
                && ACCEPTABLE_STATES.contains(ticket.getState()));
    }
}
