package by.training.novikov.ticket.service.impl.by.role.impl;

import by.training.novikov.common.exception.NotFoundException;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.exception.TicketForbiddenException;
import by.training.novikov.ticket.repository.TicketRepository;
import by.training.novikov.ticket.service.impl.by.role.TicketServiceByRole;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketServiceByEmployee implements TicketServiceByRole {

    private final TicketRepository ticketRepository;
    private final UserService userService;

    public TicketServiceByEmployee(TicketRepository ticketRepository, UserService userService) {
        this.ticketRepository = ticketRepository;
        this.userService = userService;
    }

    @Override
    public List<Ticket> getAll() {
        User user = userService.getCurrentUser();
        return ticketRepository.findAllByOwnerId(user.getId());
    }

    @Override
    public Ticket get(Long ticketId) {
        Ticket ticket = ticketRepository.findById(ticketId)
            .orElseThrow(() -> new NotFoundException("Ticket with specified id:" + ticketId + " not found"));

        if (isAvailableCredential(ticket)) {
            return ticket;
        } else {
            throw new TicketForbiddenException("Ticket forbidden for current user");
        }
    }

    @Override
    public List<Ticket> getAllMy() {
        return getAll();
    }

    @Override
    public Boolean isAvailableCredential(Ticket ticket) {
        User user = userService.getCurrentUser();
        return user.getId().equals(ticket.getOwner().getId());
    }

}
