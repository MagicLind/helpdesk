package by.training.novikov.ticket.service.impl.by.role.impl;

import by.training.novikov.common.exception.NotFoundException;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.ticket.exception.TicketForbiddenException;
import by.training.novikov.ticket.repository.TicketRepository;
import by.training.novikov.ticket.service.impl.by.role.TicketServiceByRole;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.service.UserService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TicketServiceByManager implements TicketServiceByRole {

  private static final Set<State> ACCEPTABLE_STATES = new HashSet<>();

  static {
    ACCEPTABLE_STATES.add(State.DONE);
    ACCEPTABLE_STATES.add(State.IN_PROGRESS);
    ACCEPTABLE_STATES.add(State.APPROVED);
    ACCEPTABLE_STATES.add(State.CANCELLED);
    ACCEPTABLE_STATES.add(State.DECLINED);
  }

  private final TicketRepository ticketRepository;
  private final UserService userService;

  public TicketServiceByManager(TicketRepository ticketRepository, UserService userService) {
    this.ticketRepository = ticketRepository;
    this.userService = userService;
  }

  @Override
  public List<Ticket> getAll() {

    User user = userService.getCurrentUser();

    return ticketRepository.findAllByOwnerIdOrStateOrApproverIdAndInStates(user.getId(), State.NEW, ACCEPTABLE_STATES);
  }

  @Override
  public Ticket get(Long ticketId) {
    Ticket ticket =
        ticketRepository
            .findById(ticketId)
            .orElseThrow(
                () -> new NotFoundException("Ticket with specified id:" + ticketId + " not found"));

    if (isAvailableCredential(ticket)) {
      return ticket;
    } else {
      throw new TicketForbiddenException("Ticket forbidden for current user");
    }
  }

  @Override
  public List<Ticket> getAllMy() {
    User user = userService.getCurrentUser();
    return ticketRepository.findAllByOwnerIdOrApproverIdAndByState(user.getId(), State.APPROVED);
  }

  @Override
  public Boolean isAvailableCredential(Ticket ticket) {
    User user = userService.getCurrentUser();
    return user.getId().equals(ticket.getOwner().getId())
        || (ticket.getState() == State.NEW)
        || (user.getId().equals(ticket.getApprover().getId())
            && ACCEPTABLE_STATES.contains(ticket.getState()));
  }
}
