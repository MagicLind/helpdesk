package by.training.novikov.ticket.service;

import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.dto.TicketDto;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
public interface TicketService {

  List<TicketDto> getAllDto();

  TicketDto getDto(@NotNull(message = "Tickets id should not be null") Long ticketId);

  List<TicketDto> getAllMyDto();

  TicketDto save(TicketDto ticketDto);

  TicketDto submit(TicketDto ticketDto);

  TicketDto edit(TicketDto ticketDto, @NotNull(message = "Tickets id should not be null") Long ticketId);

  TicketDto cancel(@NotNull(message = "Tickets id should not be null") Long ticketId);

  TicketDto approve(@NotNull(message = "Tickets id should not be null") Long ticketId);

  TicketDto decline(@NotNull(message = "Tickets id should not be null") Long ticketId);

  TicketDto assign(@NotNull(message = "Tickets id should not be null") Long ticketId);

  TicketDto done(@NotNull(message = "Tickets id should not be null") Long ticketId);

  Ticket getEntity(@NotNull(message = "Tickets id should not be null") Long ticketId);

  void checkCredential(@NotNull(message = "Tickets id should not be null") Long ticketId);
}
