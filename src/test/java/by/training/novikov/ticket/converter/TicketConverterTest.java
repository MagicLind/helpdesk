package by.training.novikov.ticket.converter;

import by.training.novikov.common.config.AppConfig;
import by.training.novikov.common.config.CacheConfiguration;
import by.training.novikov.common.security.SecurityConfig;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.ticket.domain.enums.Urgency;
import by.training.novikov.ticket.dto.TicketDto;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.dto.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {AppConfig.class, SecurityConfig.class, CacheConfiguration.class})
public class TicketConverterTest {

  private static final Long someTicketId = 1L;

  @Autowired
  private TicketConverter ticketConverter;

  private Ticket ticket;
  private TicketDto ticketDto;

  @Before
  public void setUp() {
    User user = new User();
    UserDto userDto = new UserDto();

    ticket = Ticket.newBuilder()
      .setName("foolish")
      .setDescription("foolish is everywhere")
      .setUrgency(Urgency.CRITICAL)
      .setState(State.NEW)
      .setOwner(user)
      .build();

    ticketDto = TicketDto.newBuilder()
      .setName("foolish")
      .setDescription("foolish is everywhere")
      .setUrgency(Urgency.CRITICAL)
      .setState(State.NEW)
      .setOwner(userDto)
      .build();
  }

  @Test
  public void fromEntity() {
    TicketDto expected = ticketDto;
    TicketDto actual = ticketConverter.fromEntity(ticket);
    assertEquals(expected, actual);
  }

  @Test
  public void toEntity() {
    Ticket expected = ticket;
    expected.setCreatedOn(null);
    Ticket actual = ticketConverter.toEntity(ticketDto, ticket.getOwner());
    actual.setCreatedOn(null);
    actual.setId(someTicketId);
    assertEquals(expected, actual);
  }
}
