package by.training.novikov.ticket.controller;

import by.training.novikov.category.domain.Category;
import by.training.novikov.common.config.AppConfig;
import by.training.novikov.common.config.CacheConfiguration;
import by.training.novikov.common.security.SecurityConfig;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.ticket.domain.enums.Urgency;
import by.training.novikov.ticket.dto.TicketDto;
import by.training.novikov.ticket.repository.TicketRepository;
import by.training.novikov.user.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {AppConfig.class, SecurityConfig.class, CacheConfiguration.class})
public class TicketControllerTest {

  public static final String CONTENT_POST_REQUEST = "{\"name\":\"some\","
    + "\"description\":\"herr\","
    + "\"urgency\":\"LOW\","
    + "\"desiredResolutionDate\":\"12/12/2021\","
    + "\"category\":{"
    + "\"id\":1,"
    + "\"name\":\"Application & Services\"}"
    + "}";
  private static final ObjectMapper MAPPER = new ObjectMapper();

  @Autowired
  private WebApplicationContext wac;

  @Autowired
  private TicketRepository ticketRepository;
  @Autowired
  private UserRepository userRepository;

  private Ticket savedTicket;

  private MockMvc mockMvc;

  @Before
  public void setup() {
    DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
    this.mockMvc = builder.build();

    Category category = new Category();
    category.setId(1L);
    savedTicket = Ticket.newBuilder()
      .setName("name")
      .setDescription("description")
      .setCreatedOn(new Date())
      .setDesiredResolutionDate(new Date())
      .setUrgency(Urgency.CRITICAL)
      .setCategory(category)
      .build();
  }

  @Test
  public void submit() throws Exception {
    String token = getAuthorizationToken("user1_mogilev@yopmail.com", "Qwer1!");

    MockHttpServletRequestBuilder submit =
      MockMvcRequestBuilders.post("/api/v1/tickets/submit")
        .contentType(MediaType.APPLICATION_JSON)
        .param("Authorization", "Bearer " + token)
        .content(CONTENT_POST_REQUEST);

    this.mockMvc
      .perform(submit)
      .andExpect(status().isCreated())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$.state").value("NEW"));
  }

  @Test
  public void save() throws Exception {
    String token = getAuthorizationToken("user1_mogilev@yopmail.com", "Qwer1!");

    MockHttpServletRequestBuilder save =
      MockMvcRequestBuilders.post("/api/v1/tickets/save")
        .contentType(MediaType.APPLICATION_JSON)
        .param("Authorization", "Bearer " + token)
        .content(CONTENT_POST_REQUEST);

    this.mockMvc
      .perform(save)
      .andExpect(status().isCreated())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$.state").value("DRAFT"));
  }

  @Test
  public void edit() throws Exception {
    String token = getAuthorizationToken("user1_mogilev@yopmail.com", "Qwer1!");

    MockHttpServletRequestBuilder save =
      MockMvcRequestBuilders.post("/api/v1/tickets/save")
        .contentType(MediaType.APPLICATION_JSON)
        .param("Authorization", "Bearer " + token)
        .content(CONTENT_POST_REQUEST);

    String content = this.mockMvc
      .perform(save)
      .andReturn()
      .getResponse().getContentAsString();
    TicketDto savedTicket = MAPPER.readValue(content, TicketDto.class);


    MockHttpServletRequestBuilder edit =
      MockMvcRequestBuilders.put(String.format("/api/v1/tickets/%d/edit", savedTicket.getId()))
        .contentType(MediaType.APPLICATION_JSON)
        .param("Authorization", "Bearer " + token)
        .content(
          "{\"name\":\"editedsome\","
            + "\"description\":\"herr\","
            + "\"urgency\":\"LOW\","
            + "\"desiredResolutionDate\":\"12/12/2021\","
            + "\"category\":{"
            + "\"id\":1,"
            + "\"name\":\"Application & Services\"}"
            + "}");

    this.mockMvc
      .perform(edit)
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$.name").value("editedsome"));

  }

  @Test
  @Transactional
  public void cancel() throws Exception {
    savedTicket.setOwner(userRepository.findByEmail("user1_mogilev@yopmail.com").get());
    savedTicket.setState(State.DRAFT);
    ticketRepository.save(savedTicket);

    String token = getAuthorizationToken("user1_mogilev@yopmail.com", "Qwer1!");

    MockHttpServletRequestBuilder cancel =
      MockMvcRequestBuilders.put(String.format("/api/v1/tickets/%d/cancel", savedTicket.getId()));

    this.mockMvc
      .perform(cancel)
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$.state").value("CANCELLED"));
  }

  @Test
  @Transactional
  public void approve() throws Exception {
    savedTicket.setOwner(userRepository.findByEmail("user1_mogilev@yopmail.com").get());
    savedTicket.setState(State.NEW);
    ticketRepository.save(savedTicket);

    String token = getAuthorizationToken("manager1_mogilev@yopmail.com", "Qwer1!");

    MockHttpServletRequestBuilder approve =
      MockMvcRequestBuilders.put(String.format("/api/v1/tickets/%d/approve", savedTicket.getId()));

    this.mockMvc
      .perform(approve)
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$.state").value("APPROVED"));
  }

  @Test
  @Transactional
  public void decline() throws Exception {
    savedTicket.setOwner(userRepository.findByEmail("user1_mogilev@yopmail.com").get());
    savedTicket.setState(State.NEW);
    ticketRepository.save(savedTicket);

    String token = getAuthorizationToken("manager1_mogilev@yopmail.com", "Qwer1!");

    MockHttpServletRequestBuilder approve =
      MockMvcRequestBuilders.put(String.format("/api/v1/tickets/%d/decline", savedTicket.getId()));

    this.mockMvc
      .perform(approve)
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$.state").value("DECLINED"));
  }

  @Test
  @Transactional
  public void assign() throws Exception {
    savedTicket.setOwner(userRepository.findByEmail("user1_mogilev@yopmail.com").get());
    savedTicket.setState(State.APPROVED);
    ticketRepository.save(savedTicket);

    String token = getAuthorizationToken("engineer1_mogilev@yopmail.com", "Qwer1!");

    MockHttpServletRequestBuilder approve =
      MockMvcRequestBuilders.put(String.format("/api/v1/tickets/%d/assign", savedTicket.getId()));

    this.mockMvc
      .perform(approve)
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$.state").value("IN_PROGRESS"));
  }

  @Test
  @Transactional
  public void done() throws Exception {
    savedTicket.setOwner(userRepository.findByEmail("user1_mogilev@yopmail.com").get());
    savedTicket.setState(State.IN_PROGRESS);
    ticketRepository.save(savedTicket);

    String token = getAuthorizationToken("engineer1_mogilev@yopmail.com", "Qwer1!");

    MockHttpServletRequestBuilder approve =
      MockMvcRequestBuilders.put(String.format("/api/v1/tickets/%d/done", savedTicket.getId()));

    this.mockMvc
      .perform(approve)
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$.state").value("DONE"));
  }

  private String getAuthorizationToken(String email, String password) throws Exception {
    MockHttpServletRequestBuilder builder =
      MockMvcRequestBuilders.post("/auth/signin")
        .contentType(MediaType.APPLICATION_JSON)
        .content(String.format("{\"email\":\"%s\",\"password\":\"%s\"}", email, password));

    return this.mockMvc.perform(builder).andReturn().getResponse().getHeader("token");
  }
}
