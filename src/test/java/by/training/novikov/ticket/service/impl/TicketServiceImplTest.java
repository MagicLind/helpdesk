package by.training.novikov.ticket.service.impl;

import by.training.novikov.email.service.EmailService;
import by.training.novikov.history.service.HistoryService;
import by.training.novikov.ticket.converter.TicketConverter;
import by.training.novikov.ticket.domain.Ticket;
import by.training.novikov.ticket.domain.enums.State;
import by.training.novikov.ticket.dto.TicketDto;
import by.training.novikov.ticket.exception.TicketForbiddenException;
import by.training.novikov.ticket.repository.TicketRepository;
import by.training.novikov.ticket.service.impl.by.role.TicketServiceByRole;
import by.training.novikov.ticket.service.impl.by.role.factory.TicketServiceFactoryByRole;
import by.training.novikov.user.domain.User;
import by.training.novikov.user.domain.enums.Role;
import by.training.novikov.user.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TicketServiceImplTest {

  @InjectMocks
  private TicketServiceImpl ticketService;

  @Mock
  private UserService userService;
  @Mock
  private TicketRepository ticketRepository;
  @Mock
  private TicketConverter ticketConverter;
  @Mock
  private TicketServiceFactoryByRole ticketServiceFactoryByRole;
  @Mock
  private HistoryService historyService;
  @Mock
  private EmailService emailService;
  @Mock
  private TicketServiceByRole ticketServiceByRole;
  @Mock
  private User user;


  @Test
  public void getAllDto() {
    List<Ticket> tickets = new ArrayList<>();
    List<TicketDto> expected = new ArrayList<>();
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketServiceFactoryByRole.getTicketServiceByRole(any(Role.class)))
      .thenReturn(ticketServiceByRole);
    when(ticketServiceByRole.getAll()).thenReturn(tickets);
    when(ticketConverter.fromListEntity(tickets)).thenReturn(expected);

    List<TicketDto> actual = ticketService.getAllDto();

    verify(ticketServiceFactoryByRole, times(1)).getTicketServiceByRole(any(Role.class));
    verify(ticketServiceByRole, times(1)).getAll();
    verify(ticketConverter, times(1)).fromListEntity(tickets);

    assertEquals(expected, actual);
  }

  @Test
  public void getAllMyDto() {
    List<Ticket> tickets = new ArrayList<>();
    List<TicketDto> expected = new ArrayList<>();
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketServiceFactoryByRole.getTicketServiceByRole(any(Role.class)))
      .thenReturn(ticketServiceByRole);
    when(ticketServiceByRole.getAllMy()).thenReturn(tickets);
    when(ticketConverter.fromListEntity(tickets)).thenReturn(expected);

    List<TicketDto> actual = ticketService.getAllMyDto();

    verify(ticketServiceFactoryByRole, times(1)).getTicketServiceByRole(any(Role.class));
    verify(ticketServiceByRole, times(1)).getAllMy();
    verify(ticketConverter, times(1)).fromListEntity(tickets);

    assertEquals(expected, actual);
  }

  @Test
  public void getDto() {
    Ticket ticket = new Ticket();
    TicketDto expected = new TicketDto();
    Long someId = 1L;
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketServiceFactoryByRole.getTicketServiceByRole(any(Role.class)))
      .thenReturn(ticketServiceByRole);
    when(ticketServiceByRole.get(anyLong())).thenReturn(ticket);
    when(ticketConverter.fromEntity(ticket)).thenReturn(expected);

    TicketDto actual = ticketService.getDto(someId);

    verify(ticketServiceFactoryByRole, times(1)).getTicketServiceByRole(any(Role.class));
    verify(ticketServiceByRole, times(1)).get(anyLong());
    verify(ticketConverter, times(1)).fromEntity(ticket);

    assertEquals(expected, actual);
  }

  @Test
  public void getEntity() {
    Ticket expected = new Ticket();
    Long someId = 1L;
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketServiceFactoryByRole.getTicketServiceByRole(any(Role.class)))
      .thenReturn(ticketServiceByRole);
    when(ticketServiceByRole.get(anyLong())).thenReturn(expected);

    Ticket actual = ticketService.getEntity(someId);

    verify(ticketServiceFactoryByRole, times(1)).getTicketServiceByRole(any(Role.class));
    verify(ticketServiceByRole, times(1)).get(anyLong());

    assertEquals(expected, actual);
  }

  @Test
  public void save() {
    Ticket ticket = new Ticket();
    TicketDto expected = new TicketDto();
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketConverter.toEntity(expected, user)).thenReturn(ticket);
    when(ticketRepository.save(ticket)).thenReturn(ticket);
    when(ticketConverter.fromEntity(ticket)).thenReturn(expected);

    TicketDto actual = ticketService.save(expected);

    verify(ticketConverter, times(1)).toEntity(expected, user);
    verify(ticketRepository, times(1)).save(ticket);
    verify(historyService, times(1)).addHistory("Ticket is created", ticket);
    verify(ticketConverter, times(1)).fromEntity(ticket);

    assertEquals(expected, actual);
  }

  @Test
  public void submitWhenTicketDtoHaveId() {
    Ticket ticket = new Ticket();
    Ticket ticketAfterConvert = new Ticket();
    ticketAfterConvert.setState(State.NEW);
    ticket.setState(State.DRAFT);
    ticket.setOwner(user);
    TicketDto expected = new TicketDto();
    Long someId = 1L;
    expected.setId(someId);

    when(ticketRepository.findById(anyLong())).thenReturn(Optional.of(ticket));
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketConverter.toEntity(expected, user)).thenReturn(ticketAfterConvert);
    when(ticketRepository.update(ticketAfterConvert)).thenReturn(ticketAfterConvert);
    when(ticketConverter.fromEntity(ticketAfterConvert)).thenReturn(expected);

    TicketDto actual = ticketService.submit(expected);

    verify(ticketConverter, times(1)).toEntity(expected, user);
    verify(ticketRepository, times(1)).update(ticketAfterConvert);
    verify(historyService, times(1))
      .addHistoryTicketStatusChanged(State.DRAFT, State.NEW, ticketAfterConvert);
    verify(ticketConverter, times(1)).fromEntity(ticketAfterConvert);
    verify(emailService, times(1)).sendMessageTicketCreated(ticketAfterConvert);

    assertEquals(expected, actual);
  }

  @Test
  public void submitWhenTicketDtoHaveNotId() {
    Ticket ticket = new Ticket();
    TicketDto expected = new TicketDto();
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketConverter.toEntity(expected, user)).thenReturn(ticket);
    when(ticketRepository.save(ticket)).thenReturn(ticket);
    when(ticketConverter.fromEntity(ticket)).thenReturn(expected);

    TicketDto actual = ticketService.submit(expected);

    verify(ticketConverter, times(1)).toEntity(expected, user);
    verify(ticketRepository, times(1)).save(ticket);
    verify(historyService, times(1)).addHistory("Ticket is created", ticket);
    verify(ticketConverter, times(1)).fromEntity(ticket);
    verify(emailService, times(1)).sendMessageTicketCreated(ticket);

    assertEquals(expected, actual);
  }

  @Test
  public void edit() {
    Ticket ticket = new Ticket();
    ticket.setState(State.DRAFT);
    ticket.setOwner(user);
    TicketDto expected = new TicketDto();
    Long someId = 1L;
    when(ticketRepository.findById(anyLong())).thenReturn(Optional.of(ticket));
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketConverter.toEntity(expected, user)).thenReturn(ticket);
    when(ticketRepository.update(ticket)).thenReturn(ticket);
    when(ticketConverter.fromEntity(ticket)).thenReturn(expected);

    TicketDto actual = ticketService.edit(expected, someId);

    verify(ticketConverter, times(1)).toEntity(expected, user);
    verify(ticketRepository, times(1)).update(ticket);
    verify(historyService, times(1)).addHistory("Ticket is edited", ticket);
    verify(ticketConverter, times(1)).fromEntity(ticket);

    assertEquals(expected, actual);
  }

  @Test(expected = TicketForbiddenException.class)
  public void editWhenTicketHasStateNotDraft() {
    Ticket ticket = new Ticket();
    ticket.setState(State.NEW);
    TicketDto expected = new TicketDto();
    Long someId = 1L;
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketRepository.findById(anyLong())).thenReturn(Optional.of(ticket));
    when(ticketConverter.toEntity(expected, user)).thenReturn(ticket);
    when(ticketConverter.fromEntity(ticket)).thenReturn(expected);

    ticketService.edit(expected, someId);

    verify(ticketConverter, times(1)).toEntity(expected, user);
    verify(ticketRepository, times(1)).save(ticket);
    verify(ticketConverter, times(1)).fromEntity(ticket);
  }

  @Test
  public void cancel() {
    Ticket ticket = new Ticket();
    ticket.setOwner(user);
    ticket.setState(State.DRAFT);
    TicketDto expected = new TicketDto();
    Long someId = 1L;
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketRepository.findById(anyLong())).thenReturn(Optional.of(ticket));
    when(ticketRepository.save(ticket)).thenReturn(ticket);
    when(ticketConverter.fromEntity(ticket)).thenReturn(expected);

    TicketDto actual = ticketService.cancel(someId);

    verify(ticketRepository, times(1)).save(ticket);
    verify(historyService, times(1))
      .addHistoryTicketStatusChanged(State.DRAFT, State.CANCELLED, ticket);
    verify(ticketConverter, times(1)).fromEntity(ticket);

    assertEquals(expected, actual);
  }

  @Test
  public void approve() {
    User owner = new User();
    owner.setRole(Role.EMPLOYEE);
    Ticket ticket = new Ticket();
    ticket.setOwner(owner);
    ticket.setState(State.NEW);
    TicketDto expected = new TicketDto();
    Long someId = 1L;
    when(userService.getCurrentUser()).thenReturn(user);
    when(user.getRole()).thenReturn(Role.MANAGER);
    when(ticketRepository.findById(anyLong())).thenReturn(Optional.of(ticket));
    when(ticketRepository.save(ticket)).thenReturn(ticket);
    when(ticketConverter.fromEntity(ticket)).thenReturn(expected);

    TicketDto actual = ticketService.approve(someId);

    verify(ticketRepository, times(1)).save(ticket);
    verify(ticketConverter, times(1)).fromEntity(ticket);

    assertEquals(expected, actual);
  }

  @Test
  public void decline() {
    Ticket ticket = new Ticket();
    ticket.setOwner(new User());
    ticket.setState(State.NEW);
    TicketDto expected = new TicketDto();
    Long someId = 1L;
    when(userService.getCurrentUser()).thenReturn(user);
    when(user.getRole()).thenReturn(Role.EMPLOYEE);
    when(ticketRepository.findById(anyLong())).thenReturn(Optional.of(ticket));
    when(ticketRepository.save(ticket)).thenReturn(ticket);
    when(ticketConverter.fromEntity(ticket)).thenReturn(expected);

    TicketDto actual = ticketService.decline(someId);

    verify(ticketRepository, times(1)).save(ticket);
    verify(historyService, times(1))
      .addHistoryTicketStatusChanged(State.NEW, State.DECLINED, ticket);
    verify(ticketConverter, times(1)).fromEntity(ticket);
    verify(emailService, times(1)).sendMessageTicketDeclined(ticket);

    assertEquals(expected, actual);
  }

  @Test
  public void assign() {
    Ticket ticket = new Ticket();
    ticket.setOwner(user);
    ticket.setState(State.APPROVED);
    TicketDto expected = new TicketDto();
    Long someId = 1L;
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketRepository.findById(anyLong())).thenReturn(Optional.of(ticket));
    when(ticketRepository.save(ticket)).thenReturn(ticket);
    when(ticketConverter.fromEntity(ticket)).thenReturn(expected);

    TicketDto actual = ticketService.assign(someId);

    verify(ticketRepository, times(1)).save(ticket);
    verify(historyService, times(1))
      .addHistoryTicketStatusChanged(State.APPROVED, State.IN_PROGRESS, ticket);
    verify(ticketConverter, times(1)).fromEntity(ticket);

    assertEquals(expected, actual);
  }

  @Test
  public void done() {
    Ticket ticket = new Ticket();
    ticket.setOwner(user);
    ticket.setState(State.IN_PROGRESS);
    TicketDto expected = new TicketDto();
    Long someId = 1L;
    when(userService.getCurrentUser()).thenReturn(user);
    when(ticketRepository.findById(anyLong())).thenReturn(Optional.of(ticket));
    when(ticketRepository.save(ticket)).thenReturn(ticket);
    when(ticketConverter.fromEntity(ticket)).thenReturn(expected);

    TicketDto actual = ticketService.done(someId);

    verify(ticketRepository, times(1)).save(ticket);
    verify(historyService, times(1))
      .addHistoryTicketStatusChanged(State.IN_PROGRESS, State.DONE, ticket);
    verify(ticketConverter, times(1)).fromEntity(ticket);
    verify(emailService, times(1)).sendMessageTicketDone(ticket);

    assertEquals(expected, actual);
  }
}

