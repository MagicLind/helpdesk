import axios from "axios";
import { Component } from "react";
import authHeader from "../context/authHeader";

export default class HistoryTabel extends Component {
  constructor(props){
    super(props);
    this.state = {
      histories : []
    }
  }

  componentDidMount(){
    axios.get(`http://localhost:8081/helpdesk/api/v1/tickets/${this.props.ticketId}/histories/default`, {
        headers :  authHeader()
      }).then(response => {
        console.log(response.data);
        this.setState({histories : response.data});
      })
      .catch(e => {
        console.log(e)
      });
  }

  getAllHistories = () =>{
    axios.get(`http://localhost:8081/helpdesk/api/v1/tickets/${this.props.ticketId}/histories/all`, {
        headers :  authHeader()
      }).then(response => {
        console.log(response.data);
        this.setState({histories : response.data});
      })
      .catch(e => {
        console.log(e)
      });
  }


  render() {
    return (
      <div>
        <h4><a style={{textDecoration:"none"}} href="#" onClick={this.getAllHistories}>Show all</a></h4>
        <table className="table table-bordered pb-5">
          <thead className="thead-light">
            <tr>
                <th>Date</th>
                <th>User</th>
                <th>Action</th>
                <th>Description</th>
            </tr>
          </thead>
          <tbody>
              {this.state.histories.map((history, index) => {
                var date = new Date(history.date);
                const months = [
                  'Jan',
                  'Feb',
                  'Mar',
                  'Apr',
                  'May',
                  'Jun',
                  'Jul',
                  'Aug',
                  'Sep',
                  'Oct',
                  'Nov',
                  'Dec'
                ]
                var formatedDate = `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}
                 ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
                return (
                <tr key={index}>
                  <th>{formatedDate}</th>
                  <th>{history.user.firstName} {history.user.lastName}</th>
                  <th>{history.action}</th>
                  <th>{history.description}</th>
                </tr>
                )}
              )}
          </tbody>
        </table>
      </div>
    );
  }
}