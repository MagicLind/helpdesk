import axios from "axios";
import { Component } from "react";
import authHeader from "../context/authHeader";
import SimpleReactValidator from 'simple-react-validator';

export default class CommentTabel extends Component {
  constructor(props){
    super(props);
    this.state = {
      comments : [],
      comment : ""
    }
    this.validator = new SimpleReactValidator({
      validators: {
        comment : { 
          message: 'Comment contains of upper and lowercase English alpha characters, digits and special characters and maximum number of characters is 500',
          rule: (val, params, validator) => {
            return validator.helpers.testRegex(val,/^[a-zA-Z0-9~\."\(\),:;<>@\[\]!#\$%&'\*\+-/=\?\^_`\{\|\}]*$/) && val.length < 500; 
          },
          messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
          required: false  // optional
        }
      }
  })
}
  

  componentDidMount(){
    axios.get(`http://localhost:8081/helpdesk/api/v1/tickets/${this.props.ticketId}/comments/default`, {
        headers :  authHeader()
      }).then(response => {
        console.log(response.data);
        this.setState({comments : response.data});
      })
      .catch(e => {
        console.log(e)
      });
  }

  setComment = (evt) => {
    this.setState({comment:evt.target.value});
    console.log(this.state.comment)
  }

  getAllComments = () =>{
   
    axios.get(`http://localhost:8081/helpdesk/api/v1/tickets/${this.props.ticketId}/comments/all`, {
      headers :  authHeader()
    }).then(response => {
      console.log(response.data);
      this.setState({comments : response.data});
    })
    .catch(e => {
      console.log(e)
    });
  }

  createComment = () => {
    this.validator.hideMessages();
    if (!this.validator.allValid()) {
    
        this.validator.showMessages();
        this.forceUpdate();
        return;
    }
    
    axios.post(`http://localhost:8081/helpdesk/api/v1/tickets/${this.props.ticketId}/comments`, {
        'text': this.state.comment
    }, {
        headers : authHeader()
      }).then(result => {
            console.log(result.data)
            this.getAllComments();
            this.setState({comment:""})
      }).catch(e => {
        console.log(e.message)
      });
}


  render() {
    return (
      <div>
          <h4><a style={{textDecoration:"none"}} href="#" onClick={this.getAllComments}>Show all</a></h4>
        <table className="table table-bordered">
          <thead className="thead-light">
            <tr>
                <th>Date</th>
                <th>User</th>
                <th>Comment</th>
            </tr>
          </thead>
          <tbody>
              {this.state.comments.map((comment, index) => {
                console.log(comment)
                var date = new Date(comment.date);
                const months = [
                  'Jan',
                  'Feb',
                  'Mar',
                  'Apr',
                  'May',
                  'Jun',
                  'Jul',
                  'Aug',
                  'Sep',
                  'Oct',
                  'Nov',
                  'Dec'
                ];
                var formatedDate = `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}
                ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;

                return(
                  <tr key={index}>
                  <th>{formatedDate}</th>
                  <th>{comment.user.firstName} {comment.user.lastName}</th>
                  <th>{comment.text}</th>
              </tr>
                );
              }
              )}
          </tbody>
        </table>
          <div className="form-group row">
            <label className="col-3" >Comment:</label>
              <div className="w-30 col-6">
                <div className="row">
                  <textarea rows="4" className="form-control" value={this.state.comment} onChange={this.setComment}></textarea>
                </div>
                <div className="row">
                  {this.validator.message('comment', this.state.comment,'comment', { className: 'text-danger' })}
                </div>
                <div className="row pt-2 d-flex flex-row-reverse">
                  <button id="submit" className="btn col-5 btn-block btn-success btn-lg " onClick={this.createComment}>Enter</button>
                </div>
              </div>
            </div>
      </div>
    );
  }
}