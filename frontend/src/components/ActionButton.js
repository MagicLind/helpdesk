import React, { Component } from 'react';
import { SplitButton, Dropdown } from 'react-bootstrap';
import axios from 'axios';
import authHeader from "../context/authHeader";
import user from "../context/user";
import { Link, Redirect } from 'react-router-dom';

export class ActionButton extends Component {
constructor(props){
  super(props);
  this.state = {
    isFeedback : false,
    isViewFeedback : false,
    isLeaveFeedback : false,
    isEditTicket : false,
    action : this.props.ticket && user().id == this.props.ticket.owner.id && this.props.ticket.state == 'DRAFT' ? "submit" :
    this.props.ticket && user().id == this.props.ticket.owner.id && this.props.ticket.state == 'NEW' ? "cancel" :
    this.props.ticket && user().role == "MANAGER" && this.props.ticket.state == 'NEW' && user().id != this.props.ticket.owner.id ? "approve" :
    this.props.ticket && user().role == "ENGINEER" && this.props.ticket.state == 'APPROVED' ? "assign" : 
    this.props.ticket && user().role == "ENGINEER" && this.props.ticket.state == 'IN_PROGRESS' ? "done" : ""
  }
}

  componentDidMount(){
    if(this.props.ticket.state == 'DONE'){
      axios.get(`http://localhost:8081/helpdesk/api/v1/tickets/${this.props.ticket.id}/feedbacks`, {
        headers :  authHeader()
      }).then(result => {
          this.setState({isFeedback:true});
          this.setState({action:"view feedback"})
      }).catch(e => {
        console.log(e)
        this.setState({isFeedback:false});
      });
      if(!this.state.isFeedback  &&  this.props.ticket && user().id == this.props.ticket.owner.id) this.setState({action:"leave feedback"})
    }
  }
  
  chooseAction = (action, evt) => {
    this.setState({action:action});
  }

  act = (event) => {
    if(this.state.action == "submit"){
      console.log(this.props.ticket)
      axios.post("http://localhost:8081/helpdesk/api/v1/tickets/submit",this.props.ticket,{
        headers :  authHeader()
      }).then(result => {
        console.log(result)
        this.props.getTickets();
      }).catch(e => {
        console.log(e.message)
      });
    } else if(this.state.action == "view feedback") {
      this.setState({isViewFeedback : true});
    } else if(this.state.action == "leave feedback") {
      this.setState({isLeaveFeedback : true});
    } else if(this.state.action == "edit") {
      this.setState({isEditTicket : true});
    } else {
      console.log(authHeader())
    axios.put(`http://localhost:8081/helpdesk/api/v1/tickets/${this.props.ticket.id}/${this.state.action}`,null,{
      headers :  authHeader()
    }).then(result => {
      this.props.getTickets();
      console.log(result)
    }).catch(e => {
      console.log(e.message)
    });
    }
    this.forceUpdate();
  }
  
  render() {
    const urlLeaveFeedback = `/tickets/${this.props.ticket.id}/leave/feedback`;
    const urlFeedback = `/tickets/${this.props.ticket.id}/feedback`;
    const urlEditTicket = `/tickets/${this.props.ticket.id}/edit`;

    return (        
    <div>
      {this.state.isEditTicket && <Redirect to={urlEditTicket}></Redirect>}
      {this.state.isViewFeedback && <Redirect to={urlFeedback}></Redirect>}
      {this.state.isLeaveFeedback && <Redirect to={urlLeaveFeedback}></Redirect>}
      <SplitButton
        key='down'
        id='dropdown-button-drop-down'
        drop='down'
        variant="success"
        title={this.state.action}
        onClick={this.act}
      >
              {  this.props.ticket && (this.props.ticket.state == 'DRAFT' || this.props.ticket.state == 'DECLINED') 
              && user().id == this.props.ticket.owner.id &&
                <Dropdown.Item eventKey="submit" onSelect={this.chooseAction}>Submit</Dropdown.Item>
              }
              { (this.props.ticket && (user().id ==  this.props.ticket.owner.id
                && ( this.props.ticket.state == 'DRAFT'
                || this.props.ticket.state == 'NEW'))
                || (user().role == 'MANAGER' && this.props.ticket.state == 'NEW')
                || (user().role == 'ENGINEER' &&  this.props.ticket.state == 'APPROVED')) &&
                <Dropdown.Item eventKey="cancel" onSelect={this.chooseAction}>Cancel</Dropdown.Item>
              }

              { this.props.ticket && user().id == this.props.ticket.owner.id && this.props.ticket.state == 'DRAFT' &&
                <Dropdown.Item eventKey="edit" onSelect={this.chooseAction}>Edit</Dropdown.Item>
              }
              { this.props.ticket && (user().role == 'MANAGER' && this.props.ticket.state == 'NEW')
                && (this.props.ticket.owner.id != user().id) &&
                <Dropdown.Item eventKey="approve" onSelect={this.chooseAction}>Approve</Dropdown.Item>
              }
              { this.props.ticket && (user().role == 'MANAGER' && this.props.ticket.state == 'NEW')
                && (this.props.ticket.owner.id != user().id) &&
                <Dropdown.Item eventKey="decline" onSelect={this.chooseAction}>Decline</Dropdown.Item>
              }
              { this.props.ticket &&  (user().role == 'ENGINEER' && this.props.ticket.state == 'APPROVED') &&
                <Dropdown.Item eventKey="assign" onSelect={this.chooseAction}>Assign</Dropdown.Item>
              }
              { this.props.ticket &&  (user().role == 'ENGINEER' && this.props.ticket.state == 'IN_PROGRESS') &&
                <Dropdown.Item eventKey="done" onSelect={this.chooseAction}>Done</Dropdown.Item>
              }
            
                { this.state.isFeedback &&
                  <Dropdown.Item eventKey="view feedback" onSelect={this.chooseAction}>View feedback</Dropdown.Item>}
                {  this.props.ticket && this.props.ticket.state == 'DONE' && this.props.ticket.owner.id == user().id && !this.state.isFeedback &&
                  <Dropdown.Item eventKey="leave feedback" onSelect={this.chooseAction}>Leave feedback</Dropdown.Item>
                }
      </SplitButton>
    </div>
    );
    
  }
  
}