import React, { useState } from "react"
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import TicketList from './pages/TicketList';
import Login from "./pages/Login";
import {AuthContext} from "./context/auth";
import PrivateRoute from "./PrivateRoute";
import CreationTicket from "./pages/CreationTicket";
import TicketOverview from "./pages/TicketOverview";
import TicketFeedback from "./pages/TicketFeedback";
import TicketEdit from "./pages/TicketEdit";
import CreationTicketFeedback from "./pages/CreationTicketFeedback";

function App(props) {
  const existingTokens = JSON.parse(localStorage.getItem("user"));
  const [authTokens, setAuthTokens] = useState(existingTokens);

  const setTokens = (data) => {
    localStorage.setItem("user", JSON.stringify(data));
    setAuthTokens(data);
  }

  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <Router>
        <Route setAuthTokens={setAuthTokens} path="/login" component={Login} />
        <PrivateRoute path="/tickets/create" component={CreationTicket} /> 
        <PrivateRoute path="/tickets/:ticketId/edit" component={TicketEdit} />
        <PrivateRoute path="/ticketList" component={TicketList} />
        <PrivateRoute path="/tickets/:ticketId/overview" component={TicketOverview} />
        <PrivateRoute path="/tickets/:ticketId/leave/feedback" component={CreationTicketFeedback} />
        <PrivateRoute path="/tickets/:ticketId/feedback" component={TicketFeedback} />
      </Router>
    </AuthContext.Provider>
  );
}

export default App;