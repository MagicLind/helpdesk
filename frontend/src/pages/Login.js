import React, { useState, useReducer, Component, useRef } from "react";
import { Redirect } from "react-router-dom";
import axios from 'axios';
import { useAuth } from "../context/auth";
import SimpleReactValidator from 'simple-react-validator';

export default class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      isLoggedIn: false,
      email:"",
      password:""
    }
    this.validator = new SimpleReactValidator({
      validators:{
        email : { 
          message: 'Email is not correct or maximum number of characters is more than 100',
          rule: (val, params, validator) => {
            return validator.helpers.testRegex(val,/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) && val.length < 100; 
          },
          messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
          required: false  // optional
        },
        password : { 
          message: 'Password is required to contain at least one uppercase and one lowercase English alpha characters, one digit and one special character.	The minimum number of characters is 6. The maximum number of characters is 20',
          rule: (val, params, validator) => {
            return validator.helpers.testRegex(val,/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&\~\.\"\(\)\,\:\;\<\>\@\[\]\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}])[A-Za-z\d@$!%*?&\~\.\"\(\)\,\:\;\<\>\@\[\]\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}]{6,}$/) && val.length < 100; 
          },
          messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
          required: false  // optional
        },
      }
    });
  }

   postLogin = (event) => {
    event.preventDefault();
    this.validator.hideMessages();
    if (!this.validator.allValid()) {
    
        this.validator.showMessages();
        this.forceUpdate( );
        return;
    }

    axios.post("http://localhost:8081/helpdesk/auth/signin", {
      "email":this.state.email,
      "password":this.state.password
    }).then(result => {
        localStorage.setItem("user", JSON.stringify(result.data));
        this.setState({isLoggedIn:true});
    }).catch(e => {
      console.log(e.message)
    });
  }


  render () {
      return (
        <div  className="container login-page h-100">
          {this.state.isLoggedIn && <Redirect to="/ticketList" />}
          <div className="row align-items-center flex-column login-page">
          <div className="row">
            <div className="col">
                <h2>Login to the Help Desk</h2>
            </div>
          </div>
          <div className="col-sm-8 align-self-center">
            <div className="row align-items-center pt-4">
              <label className="col-sm-2 col-form-label" for="email">User Name:</label>
              <div className="col">
                <div className="row">
                  <input className="form-control" type="email"
                    value={this.state.email}
                    onChange={e => {
                      this.setState({email:e.target.value});
                    }}
                    placeholder="email"
                  />
                </div>
                <div className="row">
                  {this.validator.message('email', this.state.email, 'email', { className: 'text-danger' })}
                </div>
              </div>
            </div>
            <div className="row pt-2 align-items-center">
              <label className="col-sm-2 col-form-label" for="pwd">Password:</label>
              <div className="col">
                <div className="row">
                  <input className="form-control"  type="password"
                    value={this.state.password}
                    onChange={e => {
                      this.setState({password:e.target.value});
                    }}
                  placeholder="password"
                />
                </div>
                <div className="row">
                  {this.validator.message('password', this.state.password, 'password', { className: 'text-danger' })}
                </div>
            </div>
            </div>
            <div className="row flex-row-reverse mx-auto pt-3">
              <div className="w-25 ">
                <button type="button" className="btn btn-light btn-lg btn-block border" onClick={this.postLogin}>Enter</button>
              </div>
            </div>
          </div>
          </div>
          </div>
      ); 
  }
}