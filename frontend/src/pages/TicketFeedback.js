import { Component } from "react"
import axios from 'axios';
import authHeader from "../context/authHeader";
import { Link, Redirect } from "react-router-dom";

export default class CreationTicketFeedback extends Component {
    constructor(props){
        super(props);
        this.state = {
            rate : null,
            text : ""
        }
    }

    setRate = (evt) => {
        this.setState({rate:evt.target.value});
        console.log(this.state.rate);
    }

    setText = (evt) => {
        this.setState({text:evt.target.value});
        console.log(this.state.text);
    }

    componentDidMount() {
        let ticketId = this.props.match.params.ticketId;

        axios.get(`http://localhost:8081/helpdesk/api/v1/tickets/${ticketId}/feedbacks`, {
            headers :  authHeader()
          }).then(result => {
              this.setState({text:result.data.text});
              this.setState({rate:result.data.rate});
              console.log(result.data)
          }).catch(e => {
            console.log(e)
          });

          console.log(ticketId);
    }

    render () {
        return (
            <div className="container">
                <div className="container row pt-4 d-flex justify-content-between">
                    <div className="col-2">
                        <button onClick={this.props.history.goBack} className="btn btn-success btn-lg ">Back</button>
                        <h3></h3>
                    </div>
                </div>
                <div className="container row pt-5 d-flex justify-content-center">
                    <h4>Feedback</h4>
                </div>
                <div className="container row pt-5 d-flex">
                    <div className="rated-area d-flex flex-row-reverse justify-content-center align-items-center">
                        <div className="rate">
                            <div className="row">
                                <input disabled checked={this.state.rate == 5 ? "checked" : ""} type="radio" id="star-5" name="rating" value="5" />
                                <label for="star-5"></label>    
                            </div>
                            <div className="row pt-3 rate-number">
                                5
                            </div>
                        </div>
                        <div className="rate">
                            <div className="row">
                                <input disabled checked={this.state.rate == 4 ? "checked" : ""} type="radio" id="star-4" name="rating" value="4" />
                                <label for="star-4"></label>    
                            </div>
                            <div className="row pt-3 rate-number">
                                4
                            </div>
                        </div>
                        <div className="rate">
                            <div className="row">
                                <input disabled checked={this.state.rate == 3 ? "checked" : ""} type="radio" id="star-3" name="rating" value="3" />
                                <label for="star-3"></label>    
                            </div>
                            <div className="row pt-3 rate-number">
                               3
                            </div>
                        </div>
                        <div className="rate">
                            <div className="row">
                                <input disabled checked={this.state.rate == 2 ? "checked" : ""} type="radio" id="star-2" name="rating" value="2" />
                                <label for="star-2"></label>    
                            </div>
                            <div className="row pt-3 rate-number">
                                2
                            </div>
                        </div>
                        <div className="rate">
                            <div className="row">
                                <input disabled checked={this.state.rate == 1 ? "checked" : ""} type="radio" id="star-1" name="rating" value="1" />
                                <label for="star-1"></label>    
                            </div>
                            <div className="row pt-3 rate-number">
                                1
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container  row pt-5 d-flex justify-content-center">
                    <textarea disabled value={this.state.text} style={{height:"200px",width:"70%"}}></textarea>
                </div>
            </div>   
        );
    }
}