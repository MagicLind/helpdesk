import React, { Component } from "react"
import axios from 'axios';
import authHeader from "../context/authHeader";
import { Link, Redirect } from "react-router-dom";
import SimpleReactValidator from 'simple-react-validator';

export default class CreationTicket extends Component{
    constructor(props){
        super(props);
        this.state = {
            name: "",
            description: "",
            desiredResolutionDate: null,
            urgency: "LOW",
            category : null,
            allCategories : [],
            attachment : null,
            comment : "",
            isCreated : false
        };
        this.inputFileReference = React.createRef();
        this.validator = new SimpleReactValidator({
            validators: {
              name: { 
                message: 'Name contains of lowercase English alpha characters, digits and special characters and maximum number of characters is 100',
                rule: (val, params, validator) => {
                  return validator.helpers.testRegex(val,/^[a-z0-9~\."\(\),:;<>@\[\]!#\$%&'\*\+-/=\?\^_`\{\|\\}]*$/) && val.length < 100; 
                },
                messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
                required: true  // optional
              },
              description : { 
                message: 'Description contains of upper and lowercase English alpha characters, digits and special characters and maximum number of characters is 500',
                rule: (val, params, validator) => {
                  return validator.helpers.testRegex(val,/^[a-zA-Z0-9~\."\(\),:;<>@\[\]!#\$%&'\*\+-/=\?\^_`\{\|\}]*$/) && val.length < 500; 
                },
                messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
                required: false  // optional
              },
              comment : { 
                message: 'Comment contains of upper and lowercase English alpha characters, digits and special characters and maximum number of characters is 500',
                rule: (val, params, validator) => {
                  return validator.helpers.testRegex(val,/^[a-zA-Z0-9~\."\(\),:;<>@\[\]!#\$%&'\*\+-/=\?\^_`\{\|\}]*$/) && val.length < 500; 
                },
                messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
                required: false  // optional
              },
              desiredResolutionDate : { 
                message: 'Desired resolution date should look like dd/MM/yyyy and be greater than now',
                rule: (val, params, validator) => {
                    var today = new Date();
                    today.setHours(0,0,0,0);
                    var parts = val.split("/");
                    var date = new Date(parseInt(parts[2], 10),
                                    parseInt(parts[1], 10) - 1,
                                    parseInt(parts[0], 10));
                    console.log(date);
                    console.log(val);
                    console.log(today);
                  return validator.helpers.testRegex(val,/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/) && today <= date; 
                },
                messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
                required: false  // optional
              },
              attachment : { 
                message: 'The file type is not one of pdf,doc,docx,png,jpeg,jpg or size of file more than 5 mb',
                rule: (val, params, validator) => {
                  return validator.helpers.testRegex(val.name,/^.*\.(pdf)|(doc)|(docx)|(png)|(jpeg)|(jpg)$/) && val.size <  5242880; 
                },
                messageReplace: (message, params) => message.replace(':values', this.helpers.toSentence(params)),  // optional
                required: false  // optional
              }
            }
        })
    }

    componentDidMount() {
        axios.get("http://localhost:8081/helpdesk/api/v1/categories", {
            headers :  authHeader()
          }).then(result => {
            if (result.status === 200) {
              this.setAllCategories(result.data)
              this.setState({category:this.state.allCategories[0]});
            } else {
                console.log("Oops! Not 200 status code")
            }
          }).catch(e => {
            console.log(e)
          });
    }

    setIsCreated = (isCreated) =>{
        this.setState({isCreated:isCreated})
    }

    setAllCategories = (categories) =>{
        this.setState({allCategories:categories})
    }

    setName = (evt) => {
        this.setState({name:evt.target.value})
    }

    setDescription = (evt) => {
        this.setState({description:evt.target.value})
    }
    
    setDiseredResolutionDate = (evt) => {
        this.setState({desiredResolutionDate:evt.target.value})
    }
    
    setUrgency = (evt) => {
        this.setState({urgency:evt.target.value})
    }

    setCategory = (evt) => {
        let selectedIdCategory = evt.target.value;
        let category = this.state.allCategories.find(category => category.id == selectedIdCategory)
        this.setState({category:category});
    }

    setComment = (evt) => {
        this.setState({comment:evt.target.value});
    }

    setAttachment = (evt) => {
        this.setState({attachment:evt.target.files[0]});
        console.log(this.state.attachment)
        if(evt.target.files[0] != null){
            const url = window.URL.createObjectURL(evt.target.files[0]);
            const link = document.getElementById('file');
            var filename = evt.target.files[0].name;
            link.download = filename;
            link.href = url;
            link.text = filename;
        } else {
            const link = document.getElementById('file');
            link.href = "";
            link.text = "";
        }
    }

    handleBrowseFile = (evt) => {
        this.inputFileReference.current.click();
    }

    createTicket = async (evt) => {
        this.validator.hideMessages();
        if (!this.validator.allValid()) {
        
            this.validator.showMessages();
            this.forceUpdate();
            return;
        }
        console.log(this.state)
        let ticketId;

        if(evt.target.id == "save"){
            ticketId = await this.saveTicket();
            console.log("after save ticket")
        } else {
            ticketId = await this.submitTicket();
        }

        console.log(ticketId);

        if(this.state.comment !== ""){
            this.createComment(ticketId);
            console.log("after create comment")
        }
        if(this.state.attachment !== null){
            await this.createAttachment(ticketId);
        }

        if(!this.state.isError){
            this.setIsCreated(true);
        }
    }

    submitTicket = async () => {

        var data = await axios.post("http://localhost:8081/helpdesk/api/v1/tickets/submit", {
            "name": this.state.name,
            "description" : this.state.description,
            "desiredResolutionDate" : this.state.desiredResolutionDate,
            "category" : this.state.category,
            "urgency" : this.state.urgency
        }, {
            headers : authHeader()
        });

        var createdTicket = data.data;

        return createdTicket.id;
    }

    saveTicket = async () => {
        var data = await axios.post("http://localhost:8081/helpdesk/api/v1/tickets/save", {
            "name": this.state.name,
            "description" : this.state.description,
            "desiredResolutionDate" : this.state.desiredResolutionDate,
            "category" : this.state.category,
            "urgency" : this.state.urgency
        }, {
            headers : authHeader()
        });

        var createdTicket = data.data;

        return createdTicket.id;
    }

    createComment = async (ticketId) => {
        await axios.post(`http://localhost:8081/helpdesk/api/v1/comments/${ticketId}`, {
            "text": this.state.comment
        }, {
            headers : authHeader()
          }).then(result => {
            if (result.status === 201) {
                console.log("AllRight Comment")
            } else {
                console.log("Oops")
            }
          }).catch(e => {
            console.log(e.message)
          });
    }

    createAttachment = async (ticketId) => {
        let formData = new FormData();

        formData.append('file', this.state.attachment); 

        let headers = authHeader();
        headers['Content-Type'] = 'multipart/form-data';

        for (var key of formData.entries()) {
            console.log(key[0] + ', ' + key[1]);
        }

        console.log(headers);

        await axios.post(`http://localhost:8081/helpdesk/api/v1/tickets/${ticketId}/attachments`, formData, {
            headers : authHeader()
          }).then(result => {
            if (result.status === 201) {
              console.log("AllRight")
            } else {
                console.log("Oops")
            }
          }).catch(e => {
            console.log(e.message)
          });
    }

    render()
    {
        return (
            <div>
                {this.state.isCreated && <Redirect to="/ticketList" />}
            <div className="row pl-5 pt-3">
                <Link to="/ticketList"><button className="btn btn-success btn-lg ">TicketList</button></Link>
                <h3 className="col-5 ml-5">Create new Ticket</h3>
            </div>
            <div className="container">
                <div className="form-group row pt-5">
                    <label className="col-3" >Category:</label>
                    <div  className="col-5">
                        <div className="row">
                            <select className="form-control" onChange={this.setCategory}>
                                {this.state.allCategories.map((category,index) => <option key={index} value={category.id}>{category.name}</option>)}
                            </select>
                        </div>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-3" >Name:</label>
                    <div className="col-5">
                        <div className="row">
                            <input type="text" className="form-control" onChange={this.setName} ></input>
                        </div>
                        <div className="row">
                             {this.validator.message('name', this.state.name, 'required|name', { className: 'text-danger' })}
                        </div>
                    </div>
                </div>
                
                <div className="form-group row">
                    <label className="col-3" >Description:</label>
                    <div className=" w-30 col-6">
                        <div className="row">
                            <textarea rows="5" className="form-control" onChange={this.setDescription}></textarea>
                        </div>
                        <div className="row">
                            {this.validator.message('description', this.state.description, 'description', { className: 'text-danger' })}
                        </div>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-3" >Urgency:</label>
                    <div className="col-5">
                        <div className="row">
                            <select className="form-control" onChange={this.setUrgency}>
                                <option selected value="LOW">LOW</option>
                                <option value="MEDIUM">MEDIUM</option>
                                <option value="HIGH">HIGH</option>
                                <option value="CRITICAL">CRITICAL</option>    
                            </select>
                        </div>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-3" >Desired resolution date:</label>
                    <div className="col-5">
                        <div className="row">
                            <input type="text" className="form-control" onChange={this.setDiseredResolutionDate}></input>
                        </div>
                        <div className="row">
                            {this.validator.message( 'desiredResolutionDate', this.state. desiredResolutionDate, 'desiredResolutionDate', { className: 'text-danger' })}
                        </div>
                    </div>
                </div>

                <div className="form-group row">
                    <label className="col-3" >Attachment:</label>
                    <div className="col-5">
                        <div className="row">
                            <input type="file" ref={this.inputFileReference} id='inputFile' hidden onChange={this.setAttachment}/>
                            <button className="btn btn-light btn-block border btn-lg mr-5" onClick={this.handleBrowseFile} >Browse</button>
                            <span><a id="file"></a></span>
                        </div>
                        <div className="row">
                            {this.validator.message('attachment', this.state.attachment,'attachment', { className: 'text-danger' })}
                        </div>
                    </div>
                </div>

                <div className="form-group row">
                    <label className="col-3" >Comment:</label>
                    <div className="w-30 col-6">
                        <div className="row">
                            <textarea rows="4" className="form-control " onChange={this.setComment}></textarea>
                        </div>
                        <div className="row">
                            {this.validator.message('comment', this.state.comment,'comment', { className: 'text-danger' })}
                        </div>
                    </div>
                </div>
            </div>
            { this.state.isError && <h5 className="alert alert-danger">Oops!</h5> }
            <div className="d-flex pt-5  flex-row-reverse pb-5">
                <button id="submit" className="btn btn-success btn-lg mr-5" onClick={this.createTicket}>Submit</button>
                <button id="save" className="btn btn-light border btn-lg mr-5" onClick={this.createTicket}>Save as Draft</button>
            </div>
            </div>
        )
    }
}