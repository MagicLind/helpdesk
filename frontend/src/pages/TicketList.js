import React, { Component } from "react"
import { Link } from "react-router-dom";
import axios from 'axios';
import user from "../context/user";
import { ActionButton} from "../components/ActionButton"
import authHeader from "../context/authHeader";

export default class TicketList extends Component{
    constructor(props){
        super(props);
        this.state = {
            invisibleTickets : [],
            tickets : [],
            isMyTicketList : false,
            searchBy : "id",
            search : ""
        }
    }

    setTickets = (tickets) => {
        this.setState({tickets:tickets});
    }

    getMyTickets = () => {
        axios.get("http://localhost:8081/helpdesk/api/v1/tickets/my", {
            headers :  authHeader()
          }).then(result => {
            if (result.status === 200) {
              this.setTickets(result.data)
              this.setState({invisibleTickets:result.data});
              console.log(this.state.tickets)
            } else {
                console.log("Oops! Not 200 status code")
            }
          }).catch(e => {
            console.log(e)
          });
    }

    getAllTickets = () => {
        axios.get("http://localhost:8081/helpdesk/api/v1/tickets", {
            headers :  authHeader()
          }).then(result => {
            if (result.status === 200) {
              this.setTickets(result.data)
              this.setState({invisibleTickets:result.data});
              console.log(this.state.tickets)
            } else {
                console.log("Oops! Not 200 status code")
            }
          }).catch(e => {
            console.log(e)
          });
    }

    getTickets = () => {
        if(this.state.isMyTicketList){
            this.getMyTickets();
        } else{
            this.getAllTickets();
        }
    }

    componentDidMount() {
        this.getTickets();
    }

    setIsMyTicketListEnabled = (evt) => {
        evt.preventDefault()
        this.setState({isMyTicketList: true });
        this.getMyTickets();
    }

    
    setIsMyTicketListDisabled = (evt) => {
        evt.preventDefault()
        this.setState({isMyTicketList: false });
        this.getAllTickets();
    }

    handleSearchBy = (evt) => {
        this.setState({searchBy : evt.target.value});
    }

    handleInputSearch = (evt) => {
        this.setState({search : evt.target.value});
    }

    search = (evt) => {
        console.log(this.state.invisibleTickets)
        if(this.state.searchBy == "id"){
            let filteredTickets = this.state.invisibleTickets.filter( ticket => ticket.id == this.state.search || this.state.search == "");
            this.setTickets(filteredTickets);
        } else if(this.state.searchBy == "name"){
            let filteredTickets = this.state.invisibleTickets.filter( ticket => ticket.name.match(this.state.search));
            console.log(filteredTickets)
            this.setTickets(filteredTickets)
        } else if(this.state.searchBy == "desiredDate"){
            let filteredTickets = this.state.invisibleTickets.filter(ticket => ticket.desiredResolutionDate
                && this.state.search != "" ? ticket.desiredResolutionDate.match(this.state.search) 
                : this.state.search == "" && !ticket.desiredResolutionDate ? true : false);
            console.log(filteredTickets)
            this.setTickets(filteredTickets)
        } else if(this.state.searchBy == "urgency"){
            let filteredTickets = this.state.invisibleTickets.filter( ticket => ticket.urgency.match(new RegExp(this.state.search,"i")));
            console.log(filteredTickets)
            this.setTickets(filteredTickets)
        } else if(this.state.searchBy == "state"){
            let filteredTickets = this.state.invisibleTickets.filter( ticket => ticket.state.match(new RegExp(this.state.search,"i")));
            console.log(filteredTickets)
            this.setTickets(filteredTickets)
        }
        this.forceUpdate();
    }

    sort = (evt) => {
        var sortBy = evt.target.parentElement.id;
        var asc = evt.target.classList.contains('sorting-asc');
        var sortedTickets = [];
        console.log(asc + "\n SortBy:" + sortBy);

        if(sortBy == 'id'){
            if(asc){
                sortedTickets = this.state.tickets.sort((a, b) => a.id - b.id);   
            } else {
                sortedTickets = this.state.tickets.sort((a, b) => b.id - a.id);
            }
        } else if(sortBy == 'name'){
            if(asc){
                sortedTickets = this.state.tickets.sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1);   
            } else {
                sortedTickets = this.state.tickets.sort((a, b) => a.name.toLowerCase() < b.name.toLowerCase() ? 1 : -1);   
            }
        } else if(sortBy == 'urgency'){
            if(asc){
                sortedTickets = this.state.tickets.sort((a, b) => {
                    var urgencyA = a.urgency;
                    var urgencyB = b.urgency;

                    if(urgencyA == "CRITICAL" || (urgencyA == "HIGH" && urgencyB != "CRITICAL") 
                    || (urgencyA != "LOW"  && urgencyB == "LOW") 
                    ){
                        return 1;
                    } else if(urgencyB == "CRITICAL" || (urgencyA == "LOW" && urgencyB != "LOW")){
                        return -1;
                    } else {
                        return 0;
                    }
                });   
            } else {
                sortedTickets = this.state.tickets.sort((a, b) => {
                    var urgencyA = a.urgency;
                    var urgencyB = b.urgency;

                    if(urgencyA == "CRITICAL" || (urgencyA == "HIGH" && urgencyB != "CRITICAL") 
                    || (urgencyA != "LOW"  && urgencyB == "LOW") 
                    ){
                        return -1;
                    } else if(urgencyB == "CRITICAL" || (urgencyA == "LOW" && urgencyB != "LOW")){
                        return 1;
                    } else {
                        return 0;
                    }
                });    
            }
        } else if(sortBy == 'state'){
            if(asc){
                sortedTickets = this.state.tickets.sort((a, b) => a.state.toLowerCase() > b.state.toLowerCase() ? 1 : -1);   
            } else {
                sortedTickets = this.state.tickets.sort((a, b) => a.state.toLowerCase() < b.state.toLowerCase() ? 1 : -1);   
            }
        } else if(sortBy == 'desiredDate'){
            if(asc){
                sortedTickets = this.state.tickets.sort((a, b) => {
                    if(a.desiredResolutionDate == null){
                        return 1;
                    } else if(b.desiredResolutionDate == null){
                        return -1;
                    } else if(a.desiredResolutionDate == null && b.desiredResolutionDate == null){
                        return 0;
                    }

                    var partsA = a.desiredResolutionDate.split("/");
                    var dateA = new Date(parseInt(partsA[2], 10),
                    parseInt(partsA[1], 10) - 1,
                    parseInt(partsA[0], 10));
                    var partsB = b.desiredResolutionDate.split("/");
                    var dateB = new Date(parseInt(partsB[2], 10),
                    parseInt(partsB[1], 10) - 1,
                    parseInt(partsB[0], 10));

                    return dateA > dateB ? 1 : -1; 
                });   
            } else {
                sortedTickets = this.state.tickets.sort((a, b) => {
                    if(a.desiredResolutionDate == null){
                        return 1;
                    } else if(b.desiredResolutionDate == null){
                        return -1;
                    } else if(a.desiredResolutionDate == null && b.desiredResolutionDate == null){
                        return 0;
                    }

                    var partsA = a.desiredResolutionDate.split("/");
                    var dateA = new Date(parseInt(partsA[2], 10),
                    parseInt(partsA[1], 10) - 1,
                    parseInt(partsA[0], 10));
                    var partsB = b.desiredResolutionDate.split("/");
                    var dateB = new Date(parseInt(partsB[2], 10),
                    parseInt(partsB[1], 10) - 1,
                    parseInt(partsB[0], 10));

                    return dateA < dateB ? 1 : -1; 
                });    
            }
        }

        console.log(sortedTickets);
        this.setTickets(sortedTickets);
        this.forceUpdate();
    }

    componentDidMount() {
        console.log(authHeader())
        axios.get("http://localhost:8081/helpdesk/api/v1/tickets", {
            headers :  authHeader()
          }).then(result => {
            if (result.status === 200) {
              this.setTickets(result.data);
              this.setState({invisibleTickets:result.data});
              console.log(this.state.tickets);
            } else {
                console.log("Oops! Not 200 status code")
            }
          }).catch(e => {
            console.log(e)
          });
      }

    render () {
        return (
            <div className="container">
                {user().role != 'ENGINEER' ? <div className="d-flex flex-row-reverse pt-5 pb-5">
                    <Link to="/tickets/create"><button className="btn btn-success btn-lg pg">Create new ticket</button></Link>
                </div> : <div className="pt-5 pb-5"></div>}
                <div className="row pb-5">
                    <div className="col">
                        <button className={!this.state.isMyTicketList ? "btn btn-primary btn-lg pg btn-block" : "btn btn-outline-primary btn-lg pg btn-block" } onClick={this.setIsMyTicketListDisabled}>All Tickets</button>
                    </div>
                    <div className="col">
                        <button className={this.state.isMyTicketList ? "btn btn-primary btn-lg pg btn-block" : "btn btn-outline-primary btn-lg pg btn-block" } onClick={this.setIsMyTicketListEnabled}>My Tickets</button>
                    </div>
                </div>
                <div className="row pb-3 container">
                    <div class="input-group">
                        <div>
                            <select className="form-control" onChange={this.handleSearchBy}>
                                <option selected value="id">id</option>
                                <option value="name">name</option>
                                <option value="desiredDate">Desired date</option>
                                <option value="urgency">Urgency</option>
                                <option value="state">Status</option>    
                            </select>
                        </div>
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" onInput={this.handleInputSearch}/>
                        <button type="button" class="btn btn-outline-primary" onClick={this.search}>search</button>
                    </div>
                </div>
            <table className="table table-bordered">
            <thead className="thead-light ">
            <tr>
                <th>
                    <div className="d-flex justify-content-between">
                        <div className="">
                            <span>ID</span>
                        </div>
                        <div id="id" className="d-flex flex-column">
                            <i className="btn sorting sorting-asc" onClick={this.sort}></i>
                            <i className="btn sorting sorting-desc" onClick={this.sort}></i>
                        </div>
                    </div>
                </th>
                <th>
                    <div className="d-flex justify-content-between">
                        <div className="">
                            <span>Name</span>
                        </div>
                        <div id="name" className="d-flex flex-column">
                            <i className="btn sorting sorting-asc " onClick={this.sort}></i>
                            <i className="btn sorting sorting-desc " onClick={this.sort}></i>
                        </div>
                    </div>
                </th>
                <th>
                    <div className="d-flex justify-content-between">
                        <div className="">
                            <span>Desired date</span>
                        </div>
                        <div id="desiredDate" className="d-flex flex-column">
                            <i className="btn sorting sorting-asc " onClick={this.sort}></i>
                            <i className="btn sorting sorting-desc " onClick={this.sort}></i>
                        </div>
                    </div>
                </th>
                <th>
                    <div className="d-flex justify-content-between">
                        <div className="">
                            <span>Urgency</span>
                        </div>
                        <div id="urgency" className="d-flex flex-column">
                            <i className="btn sorting sorting-asc " onClick={this.sort}></i>
                            <i className="btn sorting sorting-desc " onClick={this.sort}></i>
                        </div>
                    </div>
                </th>
                <th>
                    <div className="d-flex justify-content-between">
                        <div className="">
                            <span>Status</span>
                        </div>
                        <div id="state" className="d-flex flex-column">
                            <i className="btn sorting sorting-asc " onClick={this.sort}></i>
                            <i className="btn sorting sorting-desc " onClick={this.sort}></i>
                        </div>
                    </div>
                </th>
                <th>
                    <span>Action</span>
                </th>
            </tr>
            </thead>
            <tbody>
                {this.state.tickets && this.state.tickets.map((ticket, index) => 
                    <tr key={index}>
                        <th>{ticket.id}</th>
                       <th> <Link to={`/tickets/${ticket.id}/overview`} >{ticket.name}</Link></th>
                        <th>{ticket.desiredResolutionDate}</th>
                        <th>{ticket.urgency}</th>
                        <th>{ticket.state}</th>
                        <th><ActionButton getTickets={this.getTickets} ticket={ticket}/></th>
                    </tr>
                )}
            </tbody>
            </table>
            </div>
        );
    }
}