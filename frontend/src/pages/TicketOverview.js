import { Component } from "react"
import axios from 'axios';
import authHeader from "../context/authHeader";
import user from "../context/user";
import { Link } from "react-router-dom";
import HistoryTabel from "../components/HistoryTabel";
import CommentTabel from "../components/CommentTabel";

export default class TicketOverview extends Component{
    constructor(props){
        super(props);
        this.state = {
            ticket:null,
            isCommentTable:false,
            isFeedback : false
        }
    }

    componentDidMount() {
        let ticketId = this.props.match.params.ticketId;

        axios.get(`http://localhost:8081/helpdesk/api/v1/tickets/${ticketId}`, {
            headers :  authHeader()
          }).then(result => {
            if (result.status === 200) {
                console.log(result.data)
              this.setState({ticket:result.data});
            } else {
                console.log("Oops! Not 200 status code")
            }
          }).catch(e => {
            console.log(e)
          });

         this.getFeedback(ticketId);

          this.setDownloaFile(ticketId);

          console.log(ticketId);
    }

    getFeedback = (ticketId) => {
        axios.get(`http://localhost:8081/helpdesk/api/v1/tickets/${ticketId}/feedbacks`, {
            headers :  authHeader()
          }).then(result => {
              this.setState({isFeedback:true});
          }).catch(e => {
            console.log(e)
          });
    }

    setIsCommentTableEnabled = (evt) => {
        evt.preventDefault()
        this.setState({isCommentTable : true });
    }

    
    setIsCommentTableDisabled = (evt) => {
        evt.preventDefault()
        this.setState({isCommentTable : false });
    }

    setDownloaFile = (ticketId) => {
        axios.get(`http://localhost:8081/helpdesk/api/v1/tickets/${ticketId}/attachments`, {
            headers :  authHeader()
          }).then(response => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.getElementById('file');
            const [, headerLine] = response.headers['content-disposition'].split("filename=");
            let startFileNameIndex = headerLine.indexOf('"') + 1
            let endFileNameIndex = headerLine.lastIndexOf('"');
            let filename = headerLine.substring(startFileNameIndex, endFileNameIndex);
            link.download = filename;
            link.href = url;
            link.text = filename;
            console.log(filename);
          }).catch(e => {
            console.log(e)
          });
    }

    render () {
        const ticketId = this.props.match.params.ticketId;
        const urlEditTicket = `/tickets/${ticketId}/edit`;
        const urlLeaveFeedback = `/tickets/${ticketId}/leave/feedback`;
        const urlFeedback = `/tickets/${ticketId}/feedback`;
        console.log(user());

        return (
            <div className="container">
                
               { this.state.ticket && <div className="container row pt-4 d-flex justify-content-between">
                    <div className="col-2">
                        <Link to="/ticketList"><button className="btn btn-success btn-lg ">TicketList</button></Link>
                    </div>
                    <div className="col-6">
                        <h3>{this.state.ticket.name}</h3>
                        <div h-50>
                            <p>Created on: {this.state.ticket.createdOn}</p>
                            <p>Status: {this.state.ticket.state}</p>
                            <p>Urgency: {this.state.ticket.urgency}</p>
                            <p>Desired resolution date: {this.state.ticket.desiredResolutionDate}</p>
                            <p>Owner: {this.state.ticket.owner.firstName} {this.state.ticket.owner.lastName}</p>
                            {this.state.ticket.approver && <p>Approver: {this.state.ticket.approver.firstName} {this.state.ticket.approver.lastName}</p>}
                            {this.state.ticket.assignee && <p>Approver: {this.state.ticket.assignee.firstName} {this.state.ticket.assignee.lastName}</p>}
                            <p>Attachment: <a id="file"></a></p>
                            <p>Description: 
                                <div>
                                    <textarea className="w-100 h-100" disabled value={this.state.ticket.description}></textarea>
                                </div>
                            </p>
                            <p>Category: {this.state.ticket.category.name}</p>
                        </div>
                    </div>
                    <div className="d-flex flex-column pb-2 pr-0 float-right col-2">
                        { user().id == this.state.ticket.owner.id && this.state.ticket.state == 'DRAFT' && <div className="pb-2">
                            <Link  to={urlEditTicket} style={{ textDecoration: 'none' }}><button className="btn btn-success btn-lg pg btn-block">Edit</button></Link>
                        </div>}
                        { this.state.ticket.state == 'DONE' && this.state.ticket.owner.id == user().id && !this.state.isFeedback &&
                        <div>
                            <Link to={urlLeaveFeedback} style={{ textDecoration: 'none' }}><button className="btn btn-success btn-lg pg btn-block">Leave feedback</button></Link>
                        </div>
                        }
                        { this.state.isFeedback && 
                        <div>
                            <Link to={urlFeedback} style={{ textDecoration: 'none' }}><button className="btn btn-success btn-lg pg btn-block">Fedback</button></Link>
                        </div>
                        }
                    </div>
                </div>
                }

                <div className="row pt-4 pb-5">
                <div className="col">
                    <button className={!this.state.isCommentTable ? "btn btn-primary btn-lg pg btn-block" : "btn btn-outline-primary btn-lg pg btn-block" } onClick={this.setIsCommentTableDisabled}>History</button>
                </div>
                <div className="col">
                    <button className={this.state.isCommentTable ? "btn btn-primary btn-lg pg btn-block" : "btn btn-outline-primary btn-lg pg btn-block" } onClick={this.setIsCommentTableEnabled}>Comments</button>
                </div>
                </div>

            {this.state.ticket ? (this.state.isCommentTable ? <CommentTabel ticketId={this.state.ticket.id} />
             : <HistoryTabel ticketId={this.state.ticket.id}/> ) : "" }
            </div>
        );
    }

}