import { Component } from "react"
import axios from 'axios';
import authHeader from "../context/authHeader";
import { Link, Redirect } from "react-router-dom";
import SimpleReactValidator from 'simple-react-validator';

export default class CreationTicketFeedback extends Component {
    constructor(props){
        super(props);
        this.state = {
            rate : null,
            text : "",
            isCreated : false
        };
        this.validator = new SimpleReactValidator();
    }

    setRate = (evt) => {
        this.setState({rate:evt.target.value});
        console.log(this.state.rate);
    }

    setText = (evt) => {
        this.setState({text:evt.target.value});
        console.log(this.state.text);
    }

    createFeedback = () => {
        this.validator.hideMessages();
        if (!this.validator.allValid()) {
        
            this.validator.showMessages();
            this.forceUpdate();
            return;
        }
        let ticketId = this.props.match.params.ticketId;

        console.log(this.state.rate);
        axios.post(`http://localhost:8081/helpdesk/api/v1/tickets/${ticketId}/feedbacks`,this.state, {
            headers :  authHeader()
          }).then(result => {
            this.setState({isCreated : true});
          }).catch(e => {
            console.log(e)
          });

          console.log(ticketId);
    }


    render () {
        return (
            <div className="container">
                {this.state.isCreated && <Redirect to="/ticketList" />}
                <div className="container row pt-4 d-flex justify-content-between">
                    <div className="col-2">
                        <button onClick={this.props.history.goBack} className="btn btn-success btn-lg ">Back</button>
                        <h3></h3>
                    </div>
                </div>
                <div className="container row pt-5 d-flex justify-content-center">
                    <h5>Please, rate your statisfaction with the solution:</h5>
                </div>
                <div className="container row pt-5 d-flex">
                    <div className="rating-area d-flex flex-row-reverse justify-content-center align-items-center">
                        <div className="rate">
                            <div className="row">
                                <input onChange={this.setRate} type="radio" id="star-5" name="rating" value="5" />
                                <label for="star-5"></label>    
                            </div>
                            <div className="row pt-3 rate-number">
                                5
                            </div>
                        </div>
                        <div className="rate">
                            <div className="row">
                                <input onChange={this.setRate} type="radio" id="star-4" name="rating" value="4" />
                                <label for="star-4"></label>    
                            </div>
                            <div className="row pt-3 rate-number">
                                4
                            </div>
                        </div>
                        <div className="rate">
                            <div className="row">
                                <input onChange={this.setRate} type="radio" id="star-3" name="rating" value="3" />
                                <label for="star-3"></label>    
                            </div>
                            <div className="row pt-3 rate-number">
                               3
                            </div>
                        </div>
                        <div className="rate">
                            <div className="row">
                                <input onChange={this.setRate} type="radio" id="star-2" name="rating" value="2" />
                                <label for="star-2"></label>    
                            </div>
                            <div className="row pt-3 rate-number">
                                2
                            </div>
                        </div>
                        <div className="rate">
                            <div className="row">
                                <input onChange={this.setRate} type="radio" id="star-1" name="rating" value="1" />
                                <label for="star-1"></label>    
                            </div>
                            <div className="row pt-3 rate-number">
                                1
                            </div>
                        </div>
                    </div>
                    <div className="rating-area d-flex flex-row-reverse justify-content-center align-items-center">
                        <div className="d-flex">
                            {this.validator.message('rate', this.state.rate,'required', { className: 'text-danger' })}
                        </div> 
                    </div>
                </div>
                <div className="container  row pt-5 d-flex justify-content-center">
                    <textarea onInput={this.setText} style={{height:"200px",width:"70%"}}></textarea>
                </div>
                <div className="d-flex pt-5  flex-row-reverse pb-5 ">
                    <button id="submit" className="btn btn-success btn-lg mr-5" onClick={this.createFeedback}>Submit</button>
                </div>
            </div>   
        );
    }

}